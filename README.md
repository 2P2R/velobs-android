VelObs Android est une application qui s'utilise en complément d'une instance web de VelObs (e.g. http://velobs.2p2r.org/).
VelObs Android permet de créer de nouvelles observations sur le réseau cyclable ou bien de mettre à jour une observation existante en lui ajoutant un commentaire avec une éventuelle photo.

Exécuter VelObs Android sur votre appareil Android
La page d'accueil s'affiche :
    - Si vous n'avez pas déjà spécifié d'adresse mail de contact, enregistrez en une en cliquant sur le bouton "Changer le mail de suivi" (par défaut, c'est l'adresse e-mail associée au téléphone qui est pré-renseignée). Cette adresse e-mail n'est utilisée que dans le cadre de Velobs, pour le bon traitement des observations (s'il y a besoin de vous contacter pour des précisions au suejt de votre observation, et pour que vous soyez averti de l'évolution de vos observations)
Cliquer sur le bouton "Poster une observation"
Sélectionnez votre localisation, soit en utilisant :
    - le GPS
    - la carte Google Maps où vous positionnerez le marqueur à l'endroit de votre observation.
Après validation, VelObs Android interrogera la base de données de VelObs pour trouver d'éventuelles observations à proximité.
S'il existe des observations à proximité, VelObs Android vous affiche la liste de ces observations que vous pouvez consulter et mettre à jour le cas échéant
S'il n'existe pas d'observation à proximité, il faut créer une nouvelle observation en spécifiant :
    - Catégorie de l'observation (manque de support vélo, absence d'aménagement, détérioration...)
    - Lieu de l'observation (viendra en complément de votre localisation GPS), avec le nom de la voie et éventuellement un repère (qui peut être un numéro de rue ou tout autre détail permettant de localiser rapidement l'endroit pas les services techniques)
    - Description de l'observation
    - Proposition d'amélioration concernant votre observation
    - Ajouter éventuellement une photo éventuellement

Au plus les informations sont précises et détaillées, au plus la chance que l'observation soit traitée efficacement par les services techniques est grande. De même, ajouter systématiquement une photo à l'observation permet de faciliter le travail des personnes qui modèrent et qui traitent l'observation par la suite.
Dans le cas où vous n'avez pas de photo quand vous rentrez l'observation, vous pouvez tout à fait en rajouter une ultérieurement soit par Vl'interface web de VelObs soit grâce à VelObs Android (à partir du moment où l'observation est modérée par les bénévoles de l'association).

Un courriel vous est envoyé lors de la création de votre observation, rappelant les informations que vous avez spécifiées. Un courriel vous sera renvoyé quand l'observation aura été modérée par l'association puis quand elle sera clôturée.