package org.deuxpiedsdeuxroues.velobs;
//Activité permettant d'afficher le détail de l'observation à proximité sélectionnée par l'utilisateur

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;

import static org.deuxpiedsdeuxroues.velobs.R.id.commentairePhotoView;
import static org.deuxpiedsdeuxroues.velobs.R.id.distancetextview;


public class ReviewProximityPoiActivity extends AppCompatActivity {
    private static final String TAG = "ReviewProxPoiActivity";
    private Comment dummy;  // for poi
    ListView lvListe = null;
    TextView distanceView = null;
    TextView desctv;
    TextView addresstv;
    TextView distancetv;
    TextView categorytv;
    TextView propositiontv;
    TextView reponseCollectiviteTv;
    ImageView photoiv;
    PoiWithCommentsListAdapter adapter;
    ArrayList maListe = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_proximity_poi);
        this.dummy = new Comment("","","", "");
        //maListe.add(0, VelobsSingleton.getInstance().poi);
        for (int j=0;j<VelobsSingleton.getInstance().poi.getListCommentaires().size();j++) {
            maListe.add(j, VelobsSingleton.getInstance().poi.getListCommentaires().get(j));
        }

        distanceView = (TextView) findViewById(distancetextview);
        distanceView.setText("Observation n°" + VelobsSingleton.getInstance().poi.getId() + " (" + VelobsSingleton.getInstance().poi.getDateCreation() + ") à " + VelobsSingleton.getInstance().poi.getDistance() + " mètres");
        addresstv = (TextView) findViewById(R.id.adresstextview);
        addresstv.setText("dans la ville de " + VelobsSingleton.getInstance().poi.getVille() + ", " + VelobsSingleton.getInstance().poi.getAddress());

        categorytv = (TextView) findViewById(R.id.categorytextview);
        categorytv.setText("Catégorie : " + VelobsSingleton.getInstance().poi.getCategory());
        desctv = (TextView) findViewById(R.id.descriptiontextview);
        desctv.setText("Description : " + VelobsSingleton.getInstance().poi.getDescription());
        propositiontv = (TextView) findViewById(R.id.propositiontextview);
        propositiontv.setText("Proposition : " + VelobsSingleton.getInstance().poi.getProposition());
        reponseCollectiviteTv = (TextView) findViewById(R.id.reponseCollectiviteTextView);
        reponseCollectiviteTv.setText("Réponse Collectivité : " + VelobsSingleton.getInstance().poi.getReponseCollectivite());
        photoiv = (ImageView) findViewById(R.id.photoView);
        if ((VelobsSingleton.getInstance().poi.getPhoto() !=  null) && (VelobsSingleton.getInstance().poi.getPhoto().trim().length() > 0)) {
            if(BuildConfig.DEBUG)
            {
                Log.d(TAG, "Trying to download image " + this.getString(R.string.url_servername) + "/resources/pictures/" + VelobsSingleton.getInstance().poi.getPhoto());
                Log.d(TAG, "Trying to download image in " + photoiv.toString());

            }
            new DownloadImageTask(photoiv, dummy)
                        .execute(this.getString(R.string.url_servername) + "/resources/pictures/" + VelobsSingleton.getInstance().poi.getPhoto());
        }
        lvListe = (ListView) findViewById(R.id.listPoiWithComments);

        adapter = new PoiWithCommentsListAdapter(this, maListe);

        lvListe.setAdapter(adapter);


        Button cancel = (Button) findViewById(R.id.prevButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                finish();

            }
        });

        Button maj = (Button) findViewById(R.id.updateButton);
        maj.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Intent myIntent = new Intent(ReviewProximityPoiActivity.this, UpdateProximityPoiActivity.class);
                ReviewProximityPoiActivity.this.startActivity(myIntent);
            }
        });

    }

    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView imageV;
        Comment cmt;

        public DownloadImageTask(ImageView imageV, Comment cmt) {
            if(BuildConfig.DEBUG)
            {
                Log.d(TAG, "DownloadImageTask - setting imageView and Comment");
            }
            this.imageV = imageV;
            this.cmt = cmt;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                if(BuildConfig.DEBUG)
                {
                    Log.e("Error", e.getMessage());
                }
                //e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null) {

                imageV.setImageBitmap(result);
                cmt.setImage(result);
                //imageV.setVisibility(View.VISIBLE);
            }
        }
    }
}

