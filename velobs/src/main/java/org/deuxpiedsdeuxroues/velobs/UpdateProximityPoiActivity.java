package org.deuxpiedsdeuxroues.velobs;
//Activité permettant de mettre à jour une observation sur VelObs en envoyant un commentaire et/ou une photo

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import org.deuxpiedsdeuxroues.velobs.picture.PictureActivity;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;


public class UpdateProximityPoiActivity extends PictureActivity {

    protected static final String TAG = "UpdatePoiActivity";

    protected ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_update_proximity_poi);


        EditText descriptionText = (EditText) findViewById(R.id.commentairetext);
        descriptionText.getBackground().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);

        Button next = (Button) findViewById(R.id.nextButton);
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                sendCommentAndPicture();
            }
        });

        Button precedent = (Button) findViewById(R.id.prevButton);
        precedent.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });



        context=this;
        onCreatePicture();
    }

    public void onPause() {
        super.onPause();
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        overridePendingTransition(0, 0);
    }

    Context ct = this ;



    final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_IND:
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.setMessage(((String) msg.obj));
                    }
                    break;
                case MSG_CNF:
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }
                    Toast.makeText(getBaseContext(),
                            "Envoi réussi! Merci de votre collaboration",
                            Toast.LENGTH_LONG).show();

                    Intent a = new Intent(ct,MainActivity.class);
                    a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(a);


                    break;
                case MSG_ERR:
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }

                    Toast.makeText(getBaseContext(),
                            "Une erreur dans l'envoi s'est produite ..." + msg.obj,
                            Toast.LENGTH_LONG).show();
                    break;
                default: // should never happen
                    break;
            }
        }
    };


    protected void sendCommentAndPicture() {
        if (BuildConfig.DEBUG){
            Log.d(TAG, "Entering sendCommentAndPicture" );
        }
        DialogInterface.OnCancelListener mProgressCanceled = new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
            }
        };

        mProgressDialog = ProgressDialog.show(this, "Veuillez patienter",
                "L'envoi de votre commentaire et/ou photo commence...", true, true, mProgressCanceled);

        final EditText descriptionText = (EditText) findViewById(R.id.commentairetext);

        Thread sendProcess = new Thread((new Runnable() {

            public void run() {
                InputStream is = null;

                Message msg = null;
                String progressBarData = "Envoi des données ...";

                msg = mHandler.obtainMessage(MSG_IND, (Object) progressBarData);

                mHandler.sendMessage(msg);
                try {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG,", withImage = " + VelobsSingleton.getInstance().withImage + " comment = " + descriptionText.getText().toString());
                    }
                    if (!VelobsSingleton.getInstance().withImage && descriptionText.getText().toString().equals("")){
                        throw new Exception("Le commentaire et l'image sont vides.");
                    }
                    String crlf = "\r\n";
                    String twoHyphens = "--";
                    String boundary =  "*****";
                    int bytesRead = 0, bytesAvailable, bufferSize;
                    int maxBufferSize = 1024 * 1024;
                    HttpURLConnection con = null;
                    URL url;


                    url = new URL(context.getString(R.string.url_servername)+"/lib/php/mobile/velObsUpdatePoi.php");
                    con = (HttpURLConnection) url.openConnection();
                    con.setReadTimeout(Integer.parseInt(context.getString(R.string.http_request_timeout)));
                    con.setConnectTimeout(15000 /* milliseconds */);
                    con.setRequestMethod("POST");
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setRequestProperty("Connection", "Keep-Alive");
                    con.setRequestProperty("Cache-Control", "no-cache");
                    con.setRequestProperty("ENCTYPE", "multipart/form-data");
                    con.setRequestProperty(
                            "Content-Type", "multipart/form-data;boundary=" + boundary);
                    if (VelobsSingleton.getInstance().withImage) {
                        con.setRequestProperty("photo1", VelobsSingleton.getInstance().imageFile.getName());
                    }

                    // Start the query
                    con.connect();
                    DataOutputStream request = new DataOutputStream(
                            con.getOutputStream());
                    request.writeBytes(twoHyphens + boundary + crlf);
                    if (VelobsSingleton.getInstance().withImage) {
                        request.writeBytes("Content-Disposition: form-data; name=\"photo1\";filename=\"" +
                                VelobsSingleton.getInstance().imageFile.getName() + "\"" + crlf);
                        request.writeBytes("Content-Type: image/jpg" + crlf);
                        //request.writeBytes("Content-Transfer-Encoding: binary" + crlf);
                        request.writeBytes(crlf);

                        FileInputStream fileInputStream = new FileInputStream(photoLocation);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        byte[] buffer = new byte[bufferSize];

                        // read file and write it into form...
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                        while (bytesRead > 0) {

                            request.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        }


                        request.writeBytes(crlf);
                        request.writeBytes(twoHyphens + boundary +
                                twoHyphens + crlf);
                    }
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"text_comment\"" + crlf);
                    request.writeBytes("Content-Type: text/plain" + crlf);
                    request.writeBytes(crlf);
                    request.write(descriptionText.getText().toString().getBytes("UTF-8"));
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"id_poi\"" + crlf);
                    request.writeBytes("Content-Type: text/plain" + crlf);
                    request.writeBytes(crlf);
                    request.writeBytes(VelobsSingleton.getInstance().poi.getId());
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"mail_comment\"" + crlf);
                    request.writeBytes("Content-Type: text/plain" + crlf);
                    request.writeBytes(crlf);
                    request.writeBytes(VelobsSingleton.getInstance().mail);
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.flush();
                    request.close();


                    is = con.getInputStream();

                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(is, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                        Log.i("Server response", line);
                    }
                    is.close();
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG,"Réponse du serveur " + sb);
                    }
                    if (!sb.toString().trim().equalsIgnoreCase("dataOK")) {
                        progressBarData = "Erreur lors de l'enregistrement dans Velobs : " + sb.toString().trim();
                        msg = mHandler.obtainMessage(MSG_ERR,
                                (Object) progressBarData);

                        mHandler.sendMessage(msg);
                    } else {

                        msg = mHandler
                                .obtainMessage(MSG_CNF,
                                        "Succès de l'envoi! Merci de votre collaboration");

                        mHandler.sendMessage(msg);
                    }




                } catch (UnsupportedEncodingException e) {
                    //e.printStackTrace();
                    if (BuildConfig.DEBUG){
                        Log.e(TAG, "UnsupportedEncodingException " + e.getMessage());
                    }
                    progressBarData = "Erreur dans l'envoi";
                    System.out.println("UnsupportedEncodingException");
                    msg = mHandler.obtainMessage(MSG_ERR,
                            (Object) progressBarData);

                    mHandler.sendMessage(msg);

                } catch(SocketException e){
                    //e.printStackTrace();
                    if (BuildConfig.DEBUG){
                        Log.e(TAG, "SocketException " + e.getMessage());
                    }
                    progressBarData = "La connexion a été fermée, veuillez recliquer sur le bouton Envoyer SVP";
                    msg = mHandler.obtainMessage(MSG_ERR,
                            (Object) progressBarData);

                    mHandler.sendMessage(msg);

                } catch (IOException e) {
                    //e.printStackTrace();
                    if (BuildConfig.DEBUG){
                        Log.e(TAG, "IOException " + e.getMessage());
                    }
                    progressBarData = "Un problème de communication avec le serveur a été rencontré.";
                    msg = mHandler.obtainMessage(MSG_ERR,
                            (Object) progressBarData);

                    mHandler.sendMessage(msg);

                }
                catch (Exception e) {
                    //e.printStackTrace();
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG,", Exception = " + e.getMessage()) ;
                    }
                    progressBarData = "Commentaire et image non modifiés?";
                    msg = mHandler.obtainMessage(MSG_ERR,
                            (Object) progressBarData);

                    mHandler.sendMessage(msg);
                }

            }
        }));
        sendProcess.start();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onSaveInstanceState");
        }
        outState.putParcelable(BITMAP_STORAGE_KEY, this.bmRotated);
        outState.putBoolean(IMAGEVIEW_VISIBILITY_STORAGE_KEY, (this.bmRotated != null) );
        if (this.photoLocation != null){
            outState.putString(BITMAP_NAME_KEY, this.photoLocation);
        }
        Bundle outstate2 = outState;
        SavedInstanceFragment.getInstance( getFragmentManager() ).pushData(outstate2 );
        outState.clear();
        super.onSaveInstanceState(outState);

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onSaveInstanceState fin");
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onRestoreInstanceState : trying to get bundle");
        }
        super.onRestoreInstanceState(savedInstanceState);
        Bundle restoredInstance = SavedInstanceFragment.getInstance( getFragmentManager() ).popData();
        if (BuildConfig.DEBUG) {

            Log.d(TAG, "onRestoreInstanceState : trying to get bundle " + restoredInstance.toString());
            Log.d(TAG, "onRestoreInstanceState : trying to get bundle " + restoredInstance.getString(BITMAP_NAME_KEY));
        }
        super.onRestoreInstanceState(savedInstanceState );
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onRestoreInstanceState : Après super.onRestoreInstanceState");
        }

        this.bmRotated = restoredInstance.getParcelable(BITMAP_STORAGE_KEY);
        this.photoLocation = restoredInstance.getString(BITMAP_NAME_KEY);
        mImageView.setImageBitmap(this.bmRotated);
        mImageView.setVisibility(
                restoredInstance.getBoolean(IMAGEVIEW_VISIBILITY_STORAGE_KEY) ?
                        ImageView.VISIBLE : ImageView.INVISIBLE
        );
        mImageView.setVisibility(ImageView.VISIBLE);
    }




}
