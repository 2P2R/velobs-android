package org.deuxpiedsdeuxroues.velobs;
// Adapteur permettant de gérer une observation et les commentaires associés)
// La liste contient l'observation  et les commentaires
// Bricolage: Le meme layout est utilisé pour l'observation et les commentaires en jouant sur la visibilité des éléments du layout
//       
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.List;

public class PoiWithCommentsListAdapter extends BaseAdapter {
    private static final String TAG = "PoiCommentsListAdapter";
    List uneListe;

    LayoutInflater inflater;

    private Context ctx;

    private Comment dummy;  // for poi

    public PoiWithCommentsListAdapter(Context context, List liste) {

        inflater = LayoutInflater.from(context);
        this.uneListe = liste;
        ctx = context;
        this.dummy = new Comment("","","", "");
    }

    @Override
    public int getCount() {
        return uneListe.size();
    }

    @Override
    public Object getItem(int arg0) {
        return uneListe.get(arg0);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "getView, position = " + position);
        }
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.poiwithcommentslist_item, null);
            holder.dateCommentaireTextView = (TextView) convertView.findViewById(R.id.dateCommentaireTextView);
            holder.commentaireTextView = (TextView) convertView.findViewById(R.id.commentaireTextView);
            holder.commentairePhotoTextView = (TextView) convertView.findViewById(R.id.commentairePhotoTextView);
            holder.commentairePhotoView = (ImageView) convertView.findViewById(R.id.commentairePhotoView);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

       /* if (position == -1){
            PointOfInterest item = (PointOfInterest) uneListe.get(position);
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "getView, PointOfInterest = " + item.getId());
            }
            holder.distancetv.setText("Observation n°" + item.getId() + " (" + item.getDateCreation() + ") à " + item.getDistance() + " mètres");
            holder.addresstv.setVisibility(View.VISIBLE);
            holder.addresstv.setText("dans la ville de " + item.getVille() + ", " + item.getAddress());
            holder.categorytv.setVisibility(View.VISIBLE);
            holder.categorytv.setText("Catégorie : " + item.getCategory());
            holder.desctv.setText("Description : " + item.getDescription());
            holder.propositiontv.setVisibility(View.VISIBLE);
            holder.propositiontv.setText("Proposition : " + item.getProposition());
            holder.reponseCollectiviteTv.setVisibility(View.VISIBLE);
            holder.reponseCollectiviteTv.setText("Réponse Collectivité : " + item.getReponseCollectivite());
            if ((item.getPhoto() !=  null) && (item.getPhoto().trim().length() > 0)) {
                holder.photoiv.setVisibility(View.VISIBLE);
                if (dummy.getImage() == null) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "getView, before DownloadImageTask  for position = " + position + ", get image "+ item.getPhoto());
                    }
                    new DownloadImageTask(holder.photoiv, dummy)
                            .execute(ctx.getString(R.string.url_servername) + "/resources/pictures/" + item.getPhoto());
                } else {
                    setBitmapIntoView(holder.photoiv,dummy.getImage());
                }
            }
            else {
                holder.photoiv.setVisibility(View.GONE);
            }
        }
        else {*/
            Comment item = (Comment) uneListe.get(position);
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "getView, Comment = " + item.getDateCreation());
            }
            holder.dateCommentaireTextView.setText("Ajouté le " + item.getDateCreation());
            String txt = "Commentaire : ";
            if (!item.getDescription().isEmpty()) {
                txt += item.getDescription();
            }
            if ((item.getPhoto() !=  null) && (item.getPhoto().trim().length() > 0)) {
                holder.commentairePhotoTextView.setText("Photo associée");

                if (item.getImage() == null) {
                    //holder.photoiv.setVisibility(View.GONE);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "getView, before DownloadImageTask  for position = " + position + ", get image "+ item.getPhoto());
                    }
                    new DownloadImageTask(holder.commentairePhotoView, item)
                            .execute(ctx.getString(R.string.url_servername) + "/resources/pictures/" + item.getPhoto());
                }
                else
                {
                    setBitmapIntoView(holder.commentairePhotoView,item.getImage());
                }
            }
            else {
                holder.commentairePhotoView.setVisibility(View.GONE);
                holder.commentairePhotoTextView.setVisibility(View.GONE);
            }
            holder.commentaireTextView.setText(txt);
       // }
        return convertView;
    }

    public void setBitmapIntoView(ImageView imageV, Bitmap result) {

        /*if (result.getHeight() < result.getWidth()) {
            imageV.requestLayout();
            DisplayMetrics displayMetrics = ctx.getResources().getDisplayMetrics();
            imageV.getLayoutParams().height = Math.round(135 * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        }*/
        imageV.setImageBitmap(result);
    }

    private class ViewHolder {

        TextView dateCommentaireTextView;
        TextView commentaireTextView;
        TextView commentairePhotoTextView;
        ImageView commentairePhotoView;
    }
    
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView imageV;
        Comment cmt;

        public DownloadImageTask(ImageView imageV, Comment cmt) {
            this.imageV = imageV;
            this.cmt = cmt;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                if(BuildConfig.DEBUG)
                {
                    Log.e("Error", e.getMessage());
                }
                //e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "getView, before onPostExecute, image viewer =  "+ imageV.toString());
                }
                setBitmapIntoView(imageV, result);
                cmt.setImage(result);
                //imageV.setVisibility(View.VISIBLE);
            }
        }
    }

}
