package org.deuxpiedsdeuxroues.velobs;
//Classe représentant une observation

import java.util.ArrayList;

public class PointOfInterest {

    private String id;
    private String category;
    private String address;
    private String distance;
    private String status;
    private String photo;
    private String ville;
    private String description;
    private String proposition;
    private String reponse_collectivite;
    private String dateCreation;
    private ArrayList<Comment> listCommentaires = new ArrayList<Comment>();

    public PointOfInterest(String id, String category, String address,
                           String distance, String status, String photo, String ville, String description, String proposition, String dateCreation,String reponse_collectivite, ArrayList<Comment> listCommentaires) {

        this.id = id ;
        this.category = category ;
        this.address = address ;
        this.distance = distance ;
        this.status = status ;
        this.photo = photo ;
        this.ville = ville ;
        this.description = description ;
        this.proposition = proposition;
        this.dateCreation = dateCreation;
        this.reponse_collectivite = reponse_collectivite;
        this.listCommentaires = listCommentaires;
    }


    public void setId(String id) {
        this.id = id ;
    }

    public void setAddress(String address) {
        this.address = address ;
    }

    public void setCategory(String category) {
        this.category = category ;
    }

    public void setDistance(String distance) {
        this.distance = distance ;
    }

    public void setStatus(String status) {
        this.status = status ;
    }

    public void setPhoto(String photo) {
        this.photo = photo ;
    }

    public void setVille(String ville){
        this.ville = ville ;
    }

    public void setDescription(String description){
        this.description = description;
    }
    public void setProposition(String proposition){
        this.proposition = proposition;
    }
    public void setReponseCollectivite(String reponse_collectivite){
        this.reponse_collectivite = reponse_collectivite;
    }
    public void setListCommentaires(ArrayList<Comment> listCommentaires) { this.listCommentaires = listCommentaires;}


    public String getAddress() {
        return address;
    }

    public String getCategory() {
        return category;
    }

    public String getDistance() {
        return distance;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public String getPhoto() {
        return photo;
    }

    public String getVille() {
        return ville;
    }

    public String getDescription() {
        return description;
    }
    public String getProposition() {
        return proposition;
    }
    public String getReponseCollectivite() {
        return reponse_collectivite;
    }
    public ArrayList<Comment> getListCommentaires() {return listCommentaires; }

    public String getDateCreation() {
        return dateCreation;
    }
    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

}
