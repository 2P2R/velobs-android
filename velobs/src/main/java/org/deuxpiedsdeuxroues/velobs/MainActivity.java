package org.deuxpiedsdeuxroues.velobs;
//Activité principale : écran d'accueil de VelObs permettant de modifier le mail de suivi, d'appeler un numéro d'urgence ou de poster une observation

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.content.pm.PackageManager;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements LocationListener {
    private static final String TAG = "MainActivity";
    boolean hasMail = false;

    Context context;
    LocationManager locationManager;
    Location loc;
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    boolean isGPS = false;
    boolean isNetwork = false;
    //boolean canGetLocation = false;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //verification que les permissions ACCESS_FINE_LOCATION et ACCESS_COARSE_LOCATION sont bien données, sinon, les demande à l'utilisateur
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Connection on, check permissions?");
            }
            if (permissionsToRequest.size() > 0) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Connection on, at least one permission is to be checked");
                }
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                        ALL_PERMISSIONS_RESULT);
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Apres requestPermissions, Permission requests");
                }

            }
        }
        setContentView(R.layout.activity_main);

        context = this;
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Entering MainActivity");
        }
        Display display = getWindowManager().getDefaultDisplay();

        int height = 0;

        height = display.getHeight();

        TextView tvcall = (TextView) findViewById(R.id.textCallService);
        if ((height > 0) && (height < 801)) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "display height between 0 and 801");
            }
            TextView tvmail = (TextView) findViewById(R.id.mailtv);
            tvmail.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
            tvcall.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        }

        Button envoi = (Button) findViewById(R.id.newRecordButton);
        envoi.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                ConnectivityManager cm =
                        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = null;
                boolean isConnected = false;
                if (cm != null) {
                    activeNetwork = cm.getActiveNetworkInfo();
                    isConnected = activeNetwork != null &&
                            activeNetwork.isConnectedOrConnecting();
                }


                if (isConnected) {

                    if (hasMail) {

                        Intent myIntent = new Intent(MainActivity.this, MapActivity.class);
                        MainActivity.this.startActivity(myIntent);
                    } else {
                        Toast.makeText(context, "Veuillez renseigner un mail de suivi", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(context, "Aucune connection internet n'est disponible ...", Toast.LENGTH_LONG).show();
                }

            }
        });

        Button sendSaved = (Button) findViewById(R.id.callServiceButton);
        if (context.getString(R.string.texteAppelServices).equals("")) {
            tvcall.setVisibility(View.GONE);
        }
        if (context.getString(R.string.texteTelephoneAppelUrgenceVelo).equals("")) {
            sendSaved.setVisibility(View.GONE);
        }
        sendSaved.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE)
                        == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + context.getString(R.string.texteTelephoneAppelUrgenceVelo)));
                    startActivity(callIntent);
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{android.Manifest.permission.CALL_PHONE},
                            MY_PERMISSIONS_REQUEST_CALL_PHONE);
                }
            }
        });
    }


    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    public void onResume() {
        super.onResume();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onResume");
        }

        //récupération de la position à utiliser pour afficher la carte
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


        if (isGPS || isNetwork) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Connection on");
            }
            // check permissions


            // get location
            getLocation();
        }
        VelobsSingleton.getInstance().reset();

        Button mailButton = (Button) findViewById(R.id.mailButton);
        mailButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Intent myIntent = new Intent(MainActivity.this, MailActivity.class);
                MainActivity.this.startActivity(myIntent);

            }
        });

        TextView tvmail = (TextView) findViewById(R.id.mailtv);
        String mail = getValue(this);
        if (mail != null) {
            tvmail.setText("Mail de suivi : " + mail);
            mailButton.setText("Changer l'e-mail de suivi");
            VelobsSingleton.getInstance().mail = mail;
            hasMail = true;
        } else {
            tvmail.setText("Vous n'avez pas d'e-mail de suivi");
            mailButton.setText("Ajouter un e-mail de suivi");
            hasMail = false;
            Intent myIntent = new Intent(MainActivity.this, MailActivity.class);
            MainActivity.this.startActivity(myIntent);
        }

    }


    public static final String PREFS_NAME = "VelobsPrefs";
    public static final String PREFS_KEY = "mail_String";

    public String getValue(Context context) {
        SharedPreferences settings;
        String text;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(PREFS_KEY, null);
        return text;
    }

    public void save(Context context, String text) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putString(PREFS_KEY, text);
        editor.commit();
    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "findUnAskedPermissions, travaille sur " + perm);
            }
            if (!hasPermission(perm)) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "findUnAskedPermissions, " + perm + " n'a pas la permission");
                }
                result.add(perm);
            }
        }

        return result;
    }

    /**
     * @param permission
     * @return
     */
    private boolean hasPermission(String permission) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "hasPermission, " + permission);
        }
        if (canAskPermission()) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "hasPermission, canAskPermission OK");
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "hasPermission, canAskPermission OK " + Build.VERSION.SDK_INT + " " + (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED));
                }
                if (permission.equals("android.permission.ACCESS_FINE_LOCATION") && checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED){
                    VelobsSingleton.getInstance().GPSPermissionOK = true;
                }
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    /**
     * @return
     */
    private boolean canAskPermission() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "canAskPermission? " + (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1));
        }
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "in onRequestPermissionsResult " + requestCode);
        }
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "in onRequestPermissionsResult, ALL_PERMISSIONS_RESULT");
                }
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("Ces permissions rendront l'utilisation de VelObs plus aisée. Cliquer sur Oui pour pouvoir donner les permissions, sinon, cliquer sur Annuler",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(
                                                        new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                           // return;
                        }
                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "No rejected permissions.");
                    }
                    VelobsSingleton.getInstance().GPSPermissionOK = true;
                    getLocation();
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "in onRequestPermissionsResult, MY_PERMISSIONS_REQUEST_CALL_PHONE");
                }
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel(context.getString(R.string.permission_ask_message),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(
                                                        new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "No rejected permissions.");
                    }

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + context.getString(R.string.texteTelephoneAppelUrgenceVelo)));
                        startActivity(callIntent);
                    }

                }
                break;
        }
    }

    public void showSettingsAlert() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans showSettingsAlert.");
        }
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Le GPS n'est pas activé");
        alertDialog.setMessage("Voulez-vous activer le GPS pour accéder aux observations à proximité?");
        alertDialog.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Non", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans showSettingsAlert, avant alertDialog.show().");
        }
        alertDialog.show();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans showSettingsAlert, après alertDialog.show().");
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Annuler", null)
                .create()
                .show();
    }

    private void updateUI(Location loc) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "updateUI");
        }
    }
    @Override
    public void onLocationChanged(Location location) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onLocationChanged " + String.valueOf(location.getLatitude()));
        }
        VelobsSingleton.getInstance().lati = String.valueOf(location.getLatitude());
        VelobsSingleton.getInstance().longi = String.valueOf(location.getLongitude());
        updateUI(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void getLocation() {
        try {
            if (VelobsSingleton.getInstance().GPSPermissionOK || Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Can get location");
                }
                if (isGPS) {
                    // from GPS
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "GPS on");
                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "GPS on, locationManager != null");
                        }
                        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (loc != null) {
                            VelobsSingleton.getInstance().lati = String.valueOf(loc.getLatitude());
                            VelobsSingleton.getInstance().longi = String.valueOf(loc.getLongitude());
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "GPS on, locationManager != null, loc != null ");
                                Log.d(TAG, "GPS on, latitude = " + String.valueOf(loc.getLatitude()));
                                Log.d(TAG, "GPS on, longitude = " + String.valueOf(loc.getLongitude()));
                            }
                            updateUI(loc);
                        }
                    }
                } else if (isNetwork) {
                    // from Network Provider
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "NETWORK_PROVIDER on");
                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null) {
                            VelobsSingleton.getInstance().lati = String.valueOf(loc.getLatitude());
                            VelobsSingleton.getInstance().longi = String.valueOf(loc.getLongitude());
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "NETWORK_PROVIDER on, locationManager != null, loc != null ");
                                Log.d(TAG, "NETWORK_PROVIDER on, latitude = " + String.valueOf(loc.getLatitude()));
                                Log.d(TAG, "NETWORK_PROVIDER on, longitude = " + String.valueOf(loc.getLongitude()));
                            }
                            updateUI(loc);
                        }
                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "NETWORK_PROVIDER off, GPS off  ");
                        Log.d(TAG, "NETWORK_PROVIDER off, GPS off, latitude from config = " + context.getString(R.string.initial_latitude));
                        Log.d(TAG, "NETWORK_PROVIDER off, GPS off, longitude from config = " + context.getString(R.string.initial_longitude));
                    }
                    VelobsSingleton.getInstance().lati = context.getString(R.string.initial_latitude);
                    VelobsSingleton.getInstance().longi = context.getString(R.string.initial_longitude);
                    updateUI(loc);
                }
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Can't get location");
                }
            }
        } catch (SecurityException e) {
            //e.printStackTrace();
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "Can't get location " + e.toString());
            }
        }
    }

}
