package org.deuxpiedsdeuxroues.velobs;
//Classe représentant un commentaire


/**
 * Created by ldn on 14/01/18.
 */
import android.graphics.Bitmap;

public class Comment {
    private String id;
    private String photo;
    private String description;
    private String dateCreation;
    private Bitmap image;

    public Comment(String id, String description, String photo, String dateCreation) {
        this.id = id ;
        this.photo = photo ;
        this.description = description ;
        this.dateCreation = dateCreation;
        this.image = null;
    }

    public void setId(String id) {
        this.id = id ;
    }

    public void setPhoto(String photo) {
        this.photo = photo ;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setImage(Bitmap image) {this.image = image; }

    public String getId() {
        return id;
    }

    public String getPhoto() {
        return photo;
    }

    public String getDescription() {
        return description;
    }

    public String getDateCreation() {return dateCreation; }

    public Bitmap getImage() {return image; }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    @Override
    public String toString() {

        return ("Le " + getDateCreation() + " " + getDescription() + " avec la photo " + getPhoto());
    }





}
