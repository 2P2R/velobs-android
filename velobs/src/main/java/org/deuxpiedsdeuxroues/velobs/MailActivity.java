package org.deuxpiedsdeuxroues.velobs;
//Activité permettant de spécifier une adresse e-mail qui sera liée aux observations / commentaires
//cette adresse est enregistrée dans les préférence de velobs
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MailActivity extends AppCompatActivity {
    private static final String TAG = "DescriptionActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail);
        if(BuildConfig.DEBUG) {
            Log.d(TAG, "Entering MailActivity");
        }
        final EditText mailText = (EditText) findViewById(R.id.mailtext);
        mailText.getBackground().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);

        if (VelobsSingleton.getInstance().mail!=null) {
            mailText.setText(VelobsSingleton.getInstance().mail);
        }


        final Context context = this ;

        Button next = (Button) findViewById(R.id.okButton);
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                if (isValidEmail(mailText.getText())) {

                    VelobsSingleton.getInstance().mail = mailText.getText().toString();
                    save(context, mailText.getText().toString());
                    if(BuildConfig.DEBUG)
                    {
                        Log.d(TAG,mailText.getText()+ "is a valid e-mail");
                        Log.d(TAG, "VelobsSingleton.getInstance().mail = " + VelobsSingleton.getInstance().mail);
                    }
                    finish();

                } else {
                    Toast.makeText(context, "Cet e-mail n'est pas valide ...", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    public static final String PREFS_NAME = "VelobsPrefs";
    public static final String PREFS_KEY = "mail_String";


    public void save(Context context, String text) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); 
        editor = settings.edit(); 

        editor.putString(PREFS_KEY, text); 
        editor.commit(); 
    }

    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }



}
