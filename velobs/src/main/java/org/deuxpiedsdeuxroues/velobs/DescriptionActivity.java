package org.deuxpiedsdeuxroues.velobs;
//Activité permettant de décrire une observation en cours de création
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class DescriptionActivity extends AppCompatActivity {
    private static final String TAG = "DescriptionActivity";
    private static Context context;
    Button nextButton;
    EditText descriptionText;
    private static final String DESCRIPTION_CONTENT = "descriptionContent";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_description);
        Button cancel = (Button) findViewById(R.id.prevButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });

        descriptionText = (EditText) findViewById(R.id.descriptiontext);
        descriptionText.getBackground().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
        descriptionText.addTextChangedListener(watcher);

        nextButton = (Button) findViewById(R.id.nextButton);
        nextButton.setEnabled(false);
        nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {


                if (descriptionText.getText().toString().equals("")){
                    Toast.makeText(context, "La description doit être renseignée.", Toast.LENGTH_LONG).show();
                }else{
                    VelobsSingleton.getInstance().desc = descriptionText.getText().toString();
                    Intent myIntent = new Intent(DescriptionActivity.this, PropositionActivity.class);
                    DescriptionActivity.this.startActivity(myIntent);
                }


            }
        });

    }

    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
    private final TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        { }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {}
        @Override
        public void afterTextChanged(Editable s) {

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Dans afterTextChanged, propositiontionText.getText().length()" + descriptionText.getText().length());
            }
            if (descriptionText.getText().equals("") || descriptionText.getText().length() < 5 ) {
                nextButton.setEnabled(false);
            } else {
                nextButton.setEnabled(true);
            }
        }
    };
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (BuildConfig.DEBUG){
            Log.d(TAG, "onSaveInstanceState");
        }
        outState.putString(DESCRIPTION_CONTENT, descriptionText.getText().toString());

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG){
            Log.d(TAG, "onRestoreInstanceState");
        }
        super.onRestoreInstanceState(savedInstanceState);
        descriptionText.setText(savedInstanceState.getString(DESCRIPTION_CONTENT));
    }
    public void onResume() {
        super.onResume();
    }
}
