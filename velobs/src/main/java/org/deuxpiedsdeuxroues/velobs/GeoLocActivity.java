package org.deuxpiedsdeuxroues.velobs;
//Activité permettant de choisir de positionner l'observation par carte (appel d'une activité dédiée) ou bien d'utiliser le GPS
//Cette activité recherche ensuite les observations à proximité et affcihe un bouton permettant de les consulter ou bien de créer une nouvelle observation

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.location.LocationListener;
import android.os.AsyncTask;

import java.net.HttpURLConnection;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


public class GeoLocActivity extends AppCompatActivity implements LocationListener {
    final int TAG_CODE_PERMISSION_LOCATION = 3;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
    LocationManager locationManager;
    Location loc;
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    boolean isGPS = false;
    boolean isNetwork = false;
    boolean canGetLocation = true;

    //private LocationManager locationManager;
    private Button proxPoiButton;
    private String responseProxPoi;
    private String latitude = null;
    private String longitude = null;

    private static Context context;
    private boolean foundGPSfix = false;
    private Message msg = null;
    private static final String TAG = "GeoLocActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "debut onCreate");
        }
        setContentView(R.layout.activity_geo_loc);

        context = this;

        VelobsSingleton.getInstance().tel = "";
    }

        public void onResume() {
        super.onResume();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "debut onResume");
        }
        String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDate = new SimpleDateFormat(DATE_FORMAT_NOW);
        VelobsSingleton.getInstance().dateObs = simpleDate.format(cal.getTime());

        //verification que les permissions ACCESS_FINE_LOCATION et ACCESS_COARSE_LOCATION sont bien données, sinon, les demande à l'utilisateur
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);
        //récupération de la position à utiliser pour afficher la carte
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);



        /*if (!isGPS && !isNetwork) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Connection off");
            }
            showSettingsAlert();
            // getLastLocation();
            VelobsSingleton.getInstance().lati = context.getString(R.string.initial_latitude);
            VelobsSingleton.getInstance().longi = context.getString(R.string.initial_longitude);
        } else {*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Connection on, check permissions?");
            }
            if (permissionsToRequest.size() > 0) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Connection on, at least one permission is to be checked");
                }
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                        ALL_PERMISSIONS_RESULT);
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Apres requestPermissions, Permission requests");
                }
                canGetLocation = true;
            }
        }

        if (isGPS || isNetwork) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Connection on");
            }
            // check permissions


            // get location
            getLocation();
        }
        //Intent myIntent = new Intent(GeoLocActivity.this, MapActivity.class);
        //GeoLocActivity.this.startActivity(myIntent);


        proxPoiButton = (Button) findViewById(R.id.proxPoi);
        proxPoiButton.setVisibility(View.INVISIBLE);


        Button cancel = (Button) findViewById(R.id.prevButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                finish();

            }
        });

        Button map = (Button) findViewById(R.id.mapButton);
        map.setOnClickListener(new View.OnClickListener() {
                                   public void onClick(View view) {
                                       Intent myIntent = new Intent(GeoLocActivity.this, MapActivity.class);
                                       GeoLocActivity.this.startActivity(myIntent);
                                   }
                               }
        );
        if (VelobsSingleton.getInstance().checkPOI) {

            VelobsSingleton.getInstance().checkPOI = false;
            Toast.makeText(GeoLocActivity.this, "Si votre observation semble nouvelle, passez à l'étape suivante", Toast.LENGTH_LONG).show();
        } else {

            if (VelobsSingleton.getInstance().lati != null && VelobsSingleton.getInstance().longi != null) {

                longitude = VelobsSingleton.getInstance().longi;
                latitude = VelobsSingleton.getInstance().lati;
            }
        }

    }

    public void onPause() {
        super.onPause();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "debut onPause");
        }
        overridePendingTransition(0, 0);
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    public static final int MSG_IND = 2;
    public static final int MSG_CNF = 1;
    public static final int MSG_ERR = 0;
    protected ProgressDialog mProgressDialog;


    final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_IND:
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.setMessage(((String) msg.obj));
                    }
                    break;
                case MSG_CNF:
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }

                    break;
                case MSG_ERR:
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }


                    break;
                default: // should never happen
                    break;
            }
        }
    };

    @Override
    public void onLocationChanged(Location location) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onLocationChanged " + String.valueOf(location.getLatitude()));
        }
        VelobsSingleton.getInstance().lati = String.valueOf(location.getLatitude());
        VelobsSingleton.getInstance().longi = String.valueOf(location.getLongitude());
        updateUI(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {}

    @Override
    public void onProviderEnabled(String s) {
        getLocation();
    }

    @Override
    public void onProviderDisabled(String s) {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    private void getLocation() {
        try {
            if (canGetLocation) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Can get location");
                }
                if (isGPS) {
                    // from GPS
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "GPS on");
                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "GPS on, locationManager != null");
                        }
                        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (loc != null) {
                            VelobsSingleton.getInstance().lati = String.valueOf(loc.getLatitude());
                            VelobsSingleton.getInstance().longi = String.valueOf(loc.getLongitude());
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "GPS on, locationManager != null, loc != null ");
                                Log.d(TAG, "GPS on, latitude = " + String.valueOf(loc.getLatitude()));
                                Log.d(TAG, "GPS on, longitude = " + String.valueOf(loc.getLongitude()));
                            }
                            updateUI(loc);
                        }
                    }
                } else if (isNetwork) {
                    // from Network Provider
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "NETWORK_PROVIDER on");
                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null) {
                            VelobsSingleton.getInstance().lati = String.valueOf(loc.getLatitude());
                            VelobsSingleton.getInstance().longi = String.valueOf(loc.getLongitude());
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "NETWORK_PROVIDER on, locationManager != null, loc != null ");
                                Log.d(TAG, "NETWORK_PROVIDER on, latitude = " + String.valueOf(loc.getLatitude()));
                                Log.d(TAG, "NETWORK_PROVIDER on, longitude = " + String.valueOf(loc.getLongitude()));
                            }
                            updateUI(loc);
                        }
                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "NETWORK_PROVIDER off, GPS off  ");
                        Log.d(TAG, "NETWORK_PROVIDER off, GPS off, latitude from config = " + context.getString(R.string.initial_latitude));
                        Log.d(TAG, "NETWORK_PROVIDER off, GPS off, longitude from config = " + context.getString(R.string.initial_longitude));
                    }
                    VelobsSingleton.getInstance().lati = context.getString(R.string.initial_latitude);
                    VelobsSingleton.getInstance().longi = context.getString(R.string.initial_longitude);
                    updateUI(loc);
                }
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Can't get location");
                }
            }
        } catch (SecurityException e) {
            //e.printStackTrace();
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "Can't get location " + e.toString());
            }
        }
    }

    private void getLastLocation() {
        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            if (BuildConfig.DEBUG) {
                Log.d(TAG, provider);
                Log.d(TAG, location == null ? "NO LastLocation" : location.toString());
            }
        } catch (SecurityException e) {
            //e.printStackTrace();
        }
    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "findUnAskedPermissions, travaille sur "+perm);
            }
            if (!hasPermission(perm)) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "findUnAskedPermissions, "+perm+ " n'a pas la permission");
                }
                result.add(perm);
            }
        }

        return result;
    }

    /**
     * @param permission
     * @return
     */
    private boolean hasPermission(String permission) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "hasPermission, "+permission);
        }
        if (canAskPermission()) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "hasPermission, canAskPermission OK");
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "hasPermission, canAskPermission OK"+ Build.VERSION.SDK_INT + " " + (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED));
                }
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    /**
     * @return
     */
    private boolean canAskPermission() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "canAskPermission? "+ (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1));
        }
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "in onRequestPermissionsResult");
        }
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "in onRequestPermissionsResult, ALL_PERMISSIONS_RESULT");
                }
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("Ces permissions rendront l'utilisation de VelObs plus aisée.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(
                                                        new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "No rejected permissions.");
                    }
                    canGetLocation = true;
                    getLocation();
                }
                break;
        }
    }

    public void showSettingsAlert() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans showSettingsAlert.");
        }
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Le GPS n'est pas activé");
        alertDialog.setMessage("Voulez-vous activer le GPS pour accéder aux observations à proximité?");
        alertDialog.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Non", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans showSettingsAlert, avant alertDialog.show().");
        }
        alertDialog.show();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans showSettingsAlert, après alertDialog.show().");
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(GeoLocActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Annuler", null)
                .create()
                .show();
    }

    private void updateUI(Location loc) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "updateUI");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }
}
