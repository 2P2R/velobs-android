package org.deuxpiedsdeuxroues.velobs;
//Activité permettant à l'utilisateur de proposer une solution pour régler le problème décrit par son observation
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class PropositionActivity extends AppCompatActivity {
    private static Context context;
    Button nextButton;
    EditText propositiontionText;
    private static final String TAG = "PropositionActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_proposition);
        Button cancel = (Button) findViewById(R.id.prevButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                finish();

            }
        });

        propositiontionText = (EditText) findViewById(R.id.propositiontext);
        propositiontionText.getBackground().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
        propositiontionText.addTextChangedListener(watcher);

        nextButton = (Button) findViewById(R.id.nextButton);
        nextButton.setEnabled(false);

        nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (propositiontionText.getText().toString().equals("")){
                    Toast.makeText(context, "La proposition doit être renseignée.", Toast.LENGTH_LONG).show();
                }else{
                    VelobsSingleton.getInstance().prop = propositiontionText.getText().toString();
                    Intent myIntent = new Intent(PropositionActivity.this, PictureAndSendActivity.class);
                    PropositionActivity.this.startActivity(myIntent);
                }
            }
        });

    }

    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
    private final TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        { }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {}
        @Override
        public void afterTextChanged(Editable s) {

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Dans afterTextChanged, propositiontionText.getText().length()" + propositiontionText.getText().length());
            }
            if (propositiontionText.getText().equals("") || propositiontionText.getText().length() < 5 ) {
                nextButton.setEnabled(false);
            } else {
                nextButton.setEnabled(true);
            }
        }
    };

}
