package org.deuxpiedsdeuxroues.velobs.picture;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import org.deuxpiedsdeuxroues.velobs.BuildConfig;
import org.deuxpiedsdeuxroues.velobs.ContentResolverUtils;
import org.deuxpiedsdeuxroues.velobs.R;
import org.deuxpiedsdeuxroues.velobs.VelobsSingleton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by adrien on 24/03/18.
 */

public abstract class PictureActivity extends AppCompatActivity {

    protected static final String TAG = "PictureActivity";

    protected static final int ACTION_TAKE_PHOTO_B = 1;
    protected static final int ACTION_SELECT_PICTURE_B = 2;
    protected Context context;
    public static final int MSG_IND = 2;
    public static final int MSG_CNF = 1;
    public static final int MSG_ERR = 0;

    protected static final String BITMAP_STORAGE_KEY = "viewbitmap";

    protected static final String BITMAP_NAME_KEY = "nameBitmap";
    protected static final String IMAGEVIEW_VISIBILITY_STORAGE_KEY = "imageviewvisibility";
    protected ImageView mImageView;
    protected Bitmap mImageBitmap;
    protected Bitmap bmRotated;
    protected String photoLocation;

    protected String mCurrentPhotoPath;

    protected static final String JPEG_FILE_PREFIX = "IMG_";
    protected static final String JPEG_FILE_SUFFIX = ".jpg";

    protected AlbumStorageDirFactory mAlbumStorageDirFactory = null;


    protected String imageFileName;
    protected File laPhoto = null;
    protected File laPhotoResized = null;

    protected ArrayList<String> permissions = new ArrayList<>();
    protected ArrayList<String> permissionsToRequest;
    protected ArrayList<String> permissionsRejected = new ArrayList<>();

    protected final static int ALL_PERMISSIONS_RESULT = 101;
    protected Button photoButton;
    protected Button selectFileButton;

    protected View popupLayout;

    protected abstract void sendCommentAndPicture();

    protected PopupWindow popupWindow;
    protected Button getPhotoButton;

    /**
     * Common initialisation
     */
    protected void onCreatePicture() {

        //verification que les permissions CAMERA  sont bien données, sinon, les demander à l'utilisateur
        permissions.add(Manifest.permission.CAMERA);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

        permissionsToRequest = findUnAskedPermissions(permissions);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Connection on, check permissions?");
            }
            if (permissionsToRequest.size() > 0) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "at least one permission is to be checked");
                }
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                        ALL_PERMISSIONS_RESULT);
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Apres requestPermissions, Permission requests");
                }

            }
        }

        mImageView = (ImageView) findViewById(R.id.photoView);

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        int screen_height = outMetrics.heightPixels;
        int screen_width = outMetrics.widthPixels;
        mImageView.getLayoutParams().height = 500;
        mImageView.getLayoutParams().width = screen_width - 50;
        //mImageView.forceLayout();
        mImageView.requestLayout();
        if (this.bmRotated != null) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "this.bmRotated n'est pas null");
            }
            mImageView.setImageBitmap(this.bmRotated);
            mImageView.setVisibility(View.VISIBLE);
        }


        //Popup de selection de photo : fichier ou appareil photo
        initPopup();

        photoButton = (Button) popupLayout.findViewById(R.id.photoPopupButton);
        selectFileButton = (Button) popupLayout.findViewById(R.id.selectPicturePopupButton);
        Button closePopupButton = (Button) popupLayout.findViewById(R.id.closePopup);

        setBtnListenerOrDisable(selectFileButton, mSelectPictureOnClickListener, Intent.ACTION_GET_CONTENT );
        setBtnListenerOrDisable(photoButton,mTakePicOnClickListener,  MediaStore.ACTION_IMAGE_CAPTURE );
        closePopupButton.setOnClickListener(   new View.OnClickListener() {
            public void onClick(View view) {
                popupWindow.dismiss(); }
        }  );

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }


        getPhotoButton = (Button) findViewById(R.id.getPhoto);
        getPhotoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                showGetPhotoPopup();
            }
        });
    }



    protected void initPopup(){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popupLayout = inflater.inflate(
                R.layout.layout_select_photo_popup,
                (ViewGroup) findViewById(R.id.popupId));

        popupWindow = new PopupWindow(popupLayout, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, true );

    }
    protected void showGetPhotoPopup() {


        popupWindow.setOutsideTouchable(false);
        //popupWindow.showAtLocation(popupLayout, Gravity.CENTER, 0, 0);
        popupWindow.showAsDropDown(getPhotoButton, 0, 0 );
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "in onRequestPermissionsResult");
        }
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "in onRequestPermissionsResult, ALL_PERMISSIONS_RESULT");
                }
                photoButton.setEnabled(true);
                selectFileButton.setEnabled(true);
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        if(Manifest.permission.CAMERA.equals(perms )  ){
                            photoButton.setEnabled(false);
                        }
                        else if(Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(perms)){
                            selectFileButton.setEnabled(false);
                        }
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel(context.getString(R.string.permission_ask_message),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(
                                                        new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            //return;
                        }
                    }
                }
                break;
        }
    }
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Annuler", null)
                .create()
                .show();
    }


    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
        return imageF;
    }

    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();

        return f;
    }

    private void setPic() throws Exception {

        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "setPic photoPath " + mCurrentPhotoPath + ", pour une largeur de targetW " + targetW + " et une hauteur de " + targetH);
        }
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        if (photoW == 0){
            throw new Exception("La photo ne peut pas être traitée.");
        }
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "setPic photoPath " + mCurrentPhotoPath + ", pour une largeur de photoW " + photoW + " et une hauteur de " + photoH);
        }
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        ExifInterface exifInterface = null;
        Integer orientation = 0;
        try {
            exifInterface = new ExifInterface(mCurrentPhotoPath);
            orientation = Integer.parseInt(exifInterface.getAttribute(ExifInterface.TAG_ORIENTATION));
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "setPic , orientation " + orientation);
            }
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "setPic , erreur " + e.getMessage());
            }
        }
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        int new_width = 0;
        int new_height = 0;

        if (photoH > photoW) {
            new_height = 1024;
            new_width = photoW * 1024 / photoH;
        } else {
            new_width = 1024;
            new_height = photoH * 1024 / photoW;
        }

        Bitmap resized = Bitmap.createScaledBitmap(bitmap, new_width, new_height, true);
        this.bmRotated = rotateBitmap(resized, orientation);
        mImageView.setImageBitmap(this.bmRotated);
        mImageView.setVisibility(View.VISIBLE);

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageResizedFileName = timeStamp + "_resized";
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "setPic , avant getAlbumDir ");
        }
        File albumF = getAlbumDir();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "setPic , après getAlbumDir ");
        }
        try {
            laPhotoResized = File.createTempFile(imageResizedFileName, JPEG_FILE_SUFFIX, albumF);
            FileOutputStream fos_resized = new FileOutputStream(laPhotoResized);
            this.bmRotated.compress(Bitmap.CompressFormat.JPEG, 100, fos_resized);
            fos_resized.flush();
            fos_resized.close();
        } catch (Exception e) {
            Log.e(TAG, "setPic , erreur lors de la manipulation de la photo " + e.toString());
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    //tourne une photo en fonction de l'orientation définie en argument
    public static Bitmap rotateBitmap(Bitmap bitmapToRotate, int orientation) {

        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmapToRotate;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmapToRotate;
        }
        try {
            Bitmap bmRotatedLocal = Bitmap.createBitmap(bitmapToRotate, 0, 0, bitmapToRotate.getWidth(), bitmapToRotate.getHeight(), matrix, true);
            bitmapToRotate.recycle();
            return bmRotatedLocal;
        } catch (OutOfMemoryError e) {
            //e.printStackTrace();
            return null;
        }
    }

    private void galleryAddPic() {

        try {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Entrée dans galleryAddPic");
            }

            VelobsSingleton.getInstance().withImage = true;
            VelobsSingleton.getInstance().imageFile = laPhotoResized;
            photoLocation = laPhotoResized.getAbsolutePath();
        } catch (Exception e) {
            String errorMessage = "Erreur lors de l'accès à votre appareil pour enregistrer une photographie. Vous pouvez contacter le support (" + context.getString(R.string.email_support) + ") en décrivant votre problème pour nous aider à améliorer cette application. ";
            Toast.makeText(getBaseContext(), errorMessage
                    ,
                    Toast.LENGTH_LONG).show();
            if (BuildConfig.DEBUG) {
                Log.e(TAG, errorMessage + " : " + e.toString());
            }
        }

    }

    protected void dispatchGetPictureIntent(int actionCode) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Entrée dans dispatchGetPictureIntent");
        }
        Intent getPictureIntent;
        File f = null;
        try {

            switch (actionCode) {
                case ACTION_TAKE_PHOTO_B:

                    getPictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    f = setUpPhotoFile();
                    mCurrentPhotoPath = f.getAbsolutePath();
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Avant dans dispatchGetPictureIntent, getUriForFile");
                    }
                    Uri contentUri = FileProvider.getUriForFile(context,
                            BuildConfig.APPLICATION_ID + ".provider",
                            f);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Avant dans dispatchGetPictureIntent, queryIntentActivities");
                    }
                    List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(getPictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "Avant dans for resInfoList, packageName = " + packageName);
                        }
                        grantUriPermission(packageName, contentUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Avant dans dispatchGetPictureIntent, dispatchGetPictureIntent.setData");
                    }
                    //context.grantUriPermission(BuildConfig.APPLICATION_ID, contentUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    //Uri contentUri = Uri.fromFile(f);
                    //takePictureIntent.setData(contentUri).addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    getPictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri);
                    laPhoto = f;

                    break;
                case ACTION_SELECT_PICTURE_B:
                    Log.d(TAG, "dispatchGetPictureIntent " + ACTION_SELECT_PICTURE_B);

                   // getPictureIntent = new Intent();

                    //getPictureIntent.setType("image/*");
                   // getPictureIntent.setAction(Intent.ACTION_GET_CONTENT);
                    getPictureIntent = new Intent(
                            Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    //startActivityForResult(i,1);
                    //getPictureIntent = Intent.createChooser(getPictureIntent, "Select Picture");
                   // startActivityForResult(getPictureIntent, Intent.CODE_PICK_IMAGE_GALLERY);

                    break;
                default:
                    getPictureIntent = null;
                    break;
            } // switch

            if (getPictureIntent != null) {
                startActivityForResult(getPictureIntent, actionCode);
            } else {
                Log.e(TAG, "Action code not known : " + actionCode);
            }

        } catch (Exception e) {
            String errorMessage = "Erreur lors de l'accès à votre appareil pour enregistrer une photographie. Vous pouvez contacter le support (" + context.getString(R.string.email_support) + ") en décrivant votre problème pour nous aider à améliorer cette application. " + e.toString();
            Toast.makeText(getBaseContext(), errorMessage
                    ,
                    Toast.LENGTH_LONG).show();
            f = null;
            mCurrentPhotoPath = null;
            if (BuildConfig.DEBUG) {
                Log.e(TAG, errorMessage + " : " + e.toString());
            }
        }
    }

    protected void handleBigCameraPhoto() throws Exception {

        if (mCurrentPhotoPath != null) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Avant setPic");
            }
            setPic();
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Avant galleryAddPic");
            }
            galleryAddPic();
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Après galleryAddPic");
            }
            mCurrentPhotoPath = null;
        }

    }


    protected Button.OnClickListener mTakePicOnClickListener =
            new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dispatchGetPictureIntent(ACTION_TAKE_PHOTO_B);
                }
            };

    protected Button.OnClickListener mSelectPictureOnClickListener =
            new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dispatchGetPictureIntent(ACTION_SELECT_PICTURE_B);
                }
            };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ACTION_TAKE_PHOTO_B: {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Dans onActivityResult " + ACTION_TAKE_PHOTO_B);
                }
                if (resultCode == RESULT_OK) {
                    try {
                        handleBigCameraPhoto();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case ACTION_SELECT_PICTURE_B: {
                if (data == null){
                    break;
                }
                Uri selectedImage = data.getData();
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "selectedImage.getAuthority() =  " + selectedImage.getAuthority());
                }
                if("com.google.android.apps.docs.storage.legacy".equals(selectedImage.getAuthority())){
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "C'est du google drive ");
                    }
                }

                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Dans onActivityResult " + ACTION_SELECT_PICTURE_B);
                }

                mCurrentPhotoPath = ContentResolverUtils.getPath(context, selectedImage);
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Dans onActivityResult, mCurrentPhotoPath = " + mCurrentPhotoPath);
                }
                try {
                    handleBigCameraPhoto();
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "Erreur lors du traitement de la photo = " + e.toString());
                    }
                }
                break;
            }
        }

        popupWindow.dismiss();
    }


    public static boolean isIntentAvailable(Context context, String action) {
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent(action);
        List<ResolveInfo> list =
                packageManager.queryIntentActivities(intent,
                        PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    protected void setBtnListenerOrDisable(
            Button btn,
            Button.OnClickListener onClickListener,
            String intentName
    ) {
        //A voir ce qu'il faut faire exactement pour la selection de photo
        if (isIntentAvailable(this, intentName) || intentName.equals(Intent.ACTION_GET_CONTENT)) {
            btn.setOnClickListener(onClickListener);
        } else {
            btn.setText(
                    getText(R.string.cannot).toString() + " " + btn.getText());
            btn.setClickable(false);
        }
    }

    protected ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "findUnAskedPermissions, travaille sur " + perm);
            }
            if (!hasPermission(perm)) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "findUnAskedPermissions, " + perm + " n'a pas la permission");
                }
                result.add(perm);
            }
        }

        return result;
    }


    private String getAlbumName() {
        return getString(R.string.album_name);
    }


    private File getAlbumDir() {
        File storageDir = null;
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "getAlbumDir, getExternalStorageState " + Environment.getExternalStorageState());
            Log.d(TAG, "getAlbumDir, MEDIA_MOUNTED " + Environment.MEDIA_MOUNTED);
        }
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "getAlbumDir, storageDir " + storageDir);
            }
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "getAlbumDir, return null ");
                        }
                        return null;
                    }
                }
            }

        }
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "getAlbumDir - return storageDir " + storageDir);
        }
        return storageDir;
    }

    /**
     * @return
     */
    public static boolean canAskPermission() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "canAskPermission? " + (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1));
        }
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     * @param permission
     * @return
     */
    public boolean hasPermission(String permission) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "hasPermission, " + permission);
        }
        if (canAskPermission()) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "hasPermission, canAskPermission OK");
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "hasPermission, canAskPermission OK" + Build.VERSION.SDK_INT + " " + (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED));
                }
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }


}
