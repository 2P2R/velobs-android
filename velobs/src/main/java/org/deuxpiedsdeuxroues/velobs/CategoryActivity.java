package org.deuxpiedsdeuxroues.velobs;
//Activité permettant d'associer une catégorie à une observation en cours de création

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class CategoryActivity extends AppCompatActivity {

    String categories ;
    RadioGroup radioGroup;
    Button next;
    private static final String TAG = "CategoryActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "debut onCreate");
        }

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                categories= null;
            } else {
                categories= extras.getString("CATEGORIES");
            }
        } else {
            categories= (String) savedInstanceState.getSerializable("CATEGORIES");
        }
        Button cancel = (Button) findViewById(R.id.prevButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });

        //CLICK NEXT
        next = (Button) findViewById(R.id.nextButton);
        next.setEnabled(false);
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int radioButtonID = radioGroup.getCheckedRadioButtonId();
                VelobsSingleton.getInstance().subCat=((Integer)radioButtonID).toString();
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "NEXT "+VelobsSingleton.getInstance().subCat);
                }

                Intent myIntent = new Intent(CategoryActivity.this, AddressActivity.class);
                CategoryActivity.this.startActivity(myIntent);
            }
        });

        radioGroup = (RadioGroup) findViewById(R.id.radiocat);
        populateRadio(radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                next.setEnabled(true);
            }
        });
    }

    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onSaveInstanceState");
        }
        outState.putString("CATEGORIES", categories);
        super.onSaveInstanceState(outState);

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onSaveInstanceState fin");
        }
    }

    private void populateRadio(RadioGroup radioGroup) {

        try {
           DocumentBuilderFactory fabrique = DocumentBuilderFactory
                    .newInstance();

            DocumentBuilder constructeur = fabrique
                    .newDocumentBuilder();

            Document document = constructeur.parse(new InputSource(new ByteArrayInputStream(categories.getBytes("utf-8"))));

            Element racine = document.getDocumentElement();
            NodeList liste = racine.getElementsByTagName("categorie");

            for (int i=0; i < liste.getLength(); i++) {
                
                NodeList liste2 = ((Element) liste.item(i))
                        .getElementsByTagName("souscategorie");

                for (int j = 0; j < liste2.getLength(); j++) {

                    SousCategorie sc = new SousCategorie(
                            ((Element) liste2.item(j))
                                    .getAttribute("id"),
                            ((Element) liste2.item(j))
                                    .getAttribute("nom"),
                            ((Element) liste.item(i))
                                    .getAttribute("id"));

                    RadioButton rb = new RadioButton(this);
                    Integer id = Integer.parseInt(sc.getIdSousCat());
                    rb.setId(id);
                    rb.setText(((Element)liste2.item(j)).getAttribute("nom"));
                    radioGroup.addView(rb);
                }
            }
        } catch (Exception e) {

        }
    }


    class SousCategorie {
        public SousCategorie(String idSousCat, String value, String idCat) {
            this.idSousCat = idSousCat;
            this.value = value;
            this.idCat = idCat;
        }

        public String getSpinnerText() {
            return value;
        }

        public String getIdSousCat() {
            return idSousCat;
        }

        public String getIdCat() {
            return idCat;
        }

        public String toString() {
            return value;
        }

        String idSousCat;
        String value;
        String idCat;
    }

}
