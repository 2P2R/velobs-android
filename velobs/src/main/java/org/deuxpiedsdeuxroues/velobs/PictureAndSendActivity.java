package org.deuxpiedsdeuxroues.velobs;
//Activité permettant de prendr unen photo (optionnel) et d'envoyer sur VelObs l'ensemble de l'observation décrite au cours des différentes activités

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.deuxpiedsdeuxroues.velobs.picture.BaseAlbumDirFactory;
import org.deuxpiedsdeuxroues.velobs.picture.FroyoAlbumDirFactory;
import org.deuxpiedsdeuxroues.velobs.picture.PictureActivity;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class PictureAndSendActivity extends PictureActivity {
    final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 2;

    protected final static String TAG = "PictureAndSendActivity";

    private String mCurrentPhotoPath;

    protected ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_picture);
        Button cancel = (Button) findViewById(R.id.prevButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });

        Button next = (Button) findViewById(R.id.nextButton);
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "avant  envoiObs");
                }
                sendCommentAndPicture();
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "apres envoiObs");
                }

            }
        });

        context = this;
        onCreatePicture();

    }

    public void onPause() {
        super.onPause();
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        overridePendingTransition(0, 0);
    }

    protected void sendCommentAndPicture() {

        DialogInterface.OnCancelListener mProgressCanceled = new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
            }
        };

        mProgressDialog = ProgressDialog.show(this, "Veuillez patienter",
                "l'envoi de votre observation commence...", true, true, mProgressCanceled);

        Thread sendProcess = new Thread((new Runnable() {

            public void run() {
                InputStream is = null;

                Message msg = null;
                String progressBarData = "Envoi des données ...";

                msg = mHandler.obtainMessage(MSG_IND, (Object) progressBarData);

                mHandler.sendMessage(msg);
                try {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Entrée");
                    }

                        String crlf = "\r\n";
                        String twoHyphens = "--";
                        String boundary =  "*****";
                        int bytesRead = 0, bytesAvailable, bufferSize;
                        int maxBufferSize = 1024 * 1024;
                        HttpURLConnection con = null;
                        URL url;

                        url = new URL(context.getString(R.string.url_servername)+"/lib/php/mobile/velObsRecord.php");
                        con = (HttpURLConnection) url.openConnection();
                        con.setReadTimeout(Integer.parseInt(context.getString(R.string.http_request_timeout)) /* milliseconds */);
                        con.setConnectTimeout(15000 /* milliseconds */);
                        con.setRequestMethod("POST");
                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestProperty("Connection", "Keep-Alive");
                        con.setRequestProperty("Cache-Control", "no-cache");
                        con.setRequestProperty("ENCTYPE", "multipart/form-data");
                        con.setRequestProperty(
                                "Content-Type", "multipart/form-data;boundary=" + boundary);
                        if (VelobsSingleton.getInstance().withImage) {
                            if (BuildConfig.DEBUG){
                                Log.d(TAG, "envoiObs, VelobsSingleton.getInstance().withImage " + VelobsSingleton.getInstance().imageFile.getName());
                            }
                            con.setRequestProperty("photo1", VelobsSingleton.getInstance().imageFile.getName());
                        }
                        

                    // Start the query
                    con.connect();
                    DataOutputStream request = new DataOutputStream(
                            con.getOutputStream());
                    // request.writeBytes(twoHyphens + boundary + twoHyphens + crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    if (VelobsSingleton.getInstance().withImage) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "envoiObs, VelobsSingleton.getInstance().withImage bis " + VelobsSingleton.getInstance().imageFile.getName());
                        }
                        request.writeBytes("Content-Disposition: form-data; name=\"photo1\";filename=\"" +
                                VelobsSingleton.getInstance().imageFile.getName() + "\"" + crlf);
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "envoiObs, après VelobsSingleton.getInstance().imageFile.getName() ");
                        }
                        request.writeBytes("Content-Type: image/jpg" + crlf);
                        //request.writeBytes("Content-Transfer-Encoding: binary" + crlf);
                        request.writeBytes(crlf);
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "envoiObs, avant new FileInputStream(photoLocation)");
                        }
                        FileInputStream fileInputStream = new FileInputStream(photoLocation);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        byte[] buffer = new byte[bufferSize];

                        // read file and write it into form...
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                        while (bytesRead > 0) {

                            request.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        }

                        request.writeBytes(crlf);
                        request.writeBytes(twoHyphens + boundary + twoHyphens + crlf);
                        request.writeBytes(twoHyphens + boundary + crlf);


                    }
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "envoiObs, avant Content-Disposition: form-data; name=\"mail_poi\"");
                    }
                    request.writeBytes("Content-Disposition: form-data; name=\"mail_poi\"" + crlf);
                    request.writeBytes("Content-Type: text/plain" + crlf);
                    request.writeBytes(crlf);
                    request.writeBytes(VelobsSingleton.getInstance().mail);
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"latitude_poi\"" + crlf);
                    request.writeBytes("Content-Type: text/plain" + crlf);
                    request.writeBytes(crlf);
                    request.writeBytes(VelobsSingleton.getInstance().lati);
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"longitude_poi\"" + crlf);
                    request.writeBytes("Content-Type: text/plain" + crlf);
                    request.writeBytes(crlf);
                    request.writeBytes(VelobsSingleton.getInstance().longi);
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"desc_poi\"" + crlf);
                    request.writeBytes("Content-Type: text/plain" + crlf);
                    request.writeBytes(crlf);
                    request.write(VelobsSingleton.getInstance().desc.toString().getBytes("UTF-8"));
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"prop_poi\"" + crlf);
                    request.writeBytes("Content-Type: text/plain" + crlf);
                    request.writeBytes(crlf);
                    request.write(VelobsSingleton.getInstance().prop.toString().getBytes("UTF-8"));
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"subcategory_id_subcategory\"" + crlf);
                    request.writeBytes("Content-Type: text/plain" + crlf);
                    request.writeBytes(crlf);
                    request.writeBytes(VelobsSingleton.getInstance().subCat);
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"geolocatemode_poi\"" + crlf);
                    request.writeBytes("Content-Type: text/plain" + crlf);
                    request.writeBytes(crlf);
                    request.writeBytes(VelobsSingleton.getInstance().typeGeoLoc);
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"tel_poi\"" + crlf);
                    request.writeBytes("Content-Type: text/plain" + crlf);
                    request.writeBytes(crlf);
                    request.writeBytes("");
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"rue_poi\"" + crlf);
                    request.writeBytes("Content-Type: text/plain" + crlf);
                    request.writeBytes(crlf);
                    request.write(VelobsSingleton.getInstance().rue.toString().getBytes("UTF-8"));
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"num_poi\"" + crlf);
                    request.writeBytes("Content-Type: text/plain" + crlf);
                    request.writeBytes(crlf);
                    request.write(VelobsSingleton.getInstance().repere.toString().getBytes("UTF-8"));
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.flush();
                    request.close();
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "envoiObs, avant getInputStream");
                    }

                    is = con.getInputStream();

                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(is, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Avant lecture réponse");
                    }
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                        if (BuildConfig.DEBUG) {
                            Log.i("Server response", line);
                        }
                    }
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Après lecture réponse" + sb.toString()+"aaa");
                        Log.d(TAG, "Après lecture réponse, sb.toString().trim().startsWith = " + sb.toString().trim().startsWith("dataOK")+"aaa");
                    }
                    is.close();

                    if (!sb.toString().trim().contains("dataOK")) {
                        progressBarData = "Erreur dans l'envoi : " + sb.toString().trim()+".";
                        msg = mHandler.obtainMessage(MSG_ERR,
                                (Object) progressBarData);

                        mHandler.sendMessage(msg);
                    } else {

                        msg = mHandler
                                .obtainMessage(MSG_CNF,
                                        "Succès de l'envoi! Merci de votre collaboration. Un e-mail vous a été envoyé, ainsi qu'aux modérateurs.");
                        mHandler.sendMessage(msg);
                    }

                } catch (UnsupportedEncodingException e) {
                    //e.printStackTrace();
                    if (BuildConfig.DEBUG) {
                        Log.e("PASA", "UnsupportedEncodingException " + e.getMessage());
                    }
                    progressBarData = "Erreur dans l'envoi";

                    msg = mHandler.obtainMessage(MSG_ERR,
                            (Object) progressBarData);

                    mHandler.sendMessage(msg);

                } catch (SocketException e) {
                    //e.printStackTrace();
                    if (BuildConfig.DEBUG) {
                        Log.e("PASA", "SocketException " + e.getMessage());
                    }
                    progressBarData = "La connexion a été fermée, veuillez recliquer sur le bouton Envoyer SVP";
                    msg = mHandler.obtainMessage(MSG_ERR,
                            (Object) progressBarData);

                    mHandler.sendMessage(msg);

                } catch (IOException e) {
                    //e.printStackTrace();
                    if (BuildConfig.DEBUG) {
                        Log.e("PASA", "IOException " + e.getMessage());
                    }
                    progressBarData = "Erreur dans l'envoi";
                    msg = mHandler.obtainMessage(MSG_ERR,
                            (Object) progressBarData);

                    mHandler.sendMessage(msg);

                } catch (Exception e) {
                    //e.printStackTrace();
                    if (BuildConfig.DEBUG) {
                        Log.e("PASA", "Exception " + e.getMessage());
                    }
                    progressBarData = "Erreur dans l'envoi";
                    msg = mHandler.obtainMessage(MSG_ERR,
                            (Object) progressBarData);

                    mHandler.sendMessage(msg);
                }

            }
        }));
        sendProcess.start();


    }

    final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_IND:
                    if (mProgressDialog != null && mProgressDialog.isShowing()) {
                        mProgressDialog.setMessage(((String) msg.obj));
                    }
                    break;
                case MSG_CNF:
                    if (mProgressDialog != null && mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }
                    Toast.makeText(getBaseContext(),
                            "Envoi réussi! Merci de votre collaboration. Un e-mail vous a été envoyé, ainsi qu'aux modérateurs.",
                            Toast.LENGTH_LONG).show();

                    Intent a = new Intent(context, MainActivity.class);
                    a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(a);


                    break;
                case MSG_ERR:
                    if (mProgressDialog != null && mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }

                    Toast.makeText(getBaseContext(),
                            msg.obj.toString(),
                            Toast.LENGTH_LONG).show();
                    break;
                default: // should never happen
                    break;
            }
        }
    };



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(BITMAP_STORAGE_KEY, this.bmRotated);
        outState.putBoolean(IMAGEVIEW_VISIBILITY_STORAGE_KEY, (this.bmRotated != null));
        if (this.photoLocation != null) {
            outState.putString(BITMAP_NAME_KEY, this.photoLocation);
        }
        SavedInstanceFragment.getInstance(getFragmentManager()).pushData(outState);
        outState.clear();
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onRestoreInstanceState : trying to get bundle");
        }
        super.onRestoreInstanceState(savedInstanceState);
        Bundle restoredInstance = SavedInstanceFragment.getInstance(getFragmentManager()).popData();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onRestoreInstanceState : trying to get bundle " + restoredInstance.toString());
            Log.d(TAG, "onRestoreInstanceState : trying to get bundle " + restoredInstance.getString(BITMAP_NAME_KEY));
        }
        super.onRestoreInstanceState(savedInstanceState);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onRestoreInstanceState : Après super.onRestoreInstanceState");
        }

        this.bmRotated = restoredInstance.getParcelable(BITMAP_STORAGE_KEY);
        this.photoLocation = restoredInstance.getString(BITMAP_NAME_KEY);
        mImageView.setImageBitmap(this.bmRotated);
        mImageView.setVisibility(
                restoredInstance.getBoolean(IMAGEVIEW_VISIBILITY_STORAGE_KEY) ?
                        ImageView.VISIBLE : ImageView.INVISIBLE
        );
        mImageView.setVisibility(ImageView.VISIBLE);
    }


}
