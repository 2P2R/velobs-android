package org.deuxpiedsdeuxroues.velobs;

//Activité affichant une carte google maps et permettant à l'utilisateur de positionner son observation sur la carte.
//on affiche les observations à proximité pour permettre à l'utilisateur de les mettre à jour ou bien d'en créer une

import android.Manifest;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class MapActivity extends FragmentActivity implements GoogleMap.OnMapClickListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener, LocationListener {
    private GoogleMap mMap;
    private Marker leMarker = null;
    private Circle circle = null;
    private static Context context;
    private static final String TAG = "MapActivity";
    private Message msg;
    private String responseProxPoi;
    HashMap<Integer, PointOfInterest> hashObservations = new HashMap<Integer, PointOfInterest>();
    private static boolean categoriesDownloaded = false;
    private boolean doublonsDownloaded = false;
    private static String categories;
    LocationManager locationManager;
    boolean isGPS = false;
    boolean isNetwork = false;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 5;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 10 * 1;
    Location loc;
    private boolean bUserAnswerGPSToActivateAsked = false;
    private static final String ASKED_USER_ACTIVATE_GPS = "askedUserActivateGPS";
    private boolean bMessageAfterOnMapClickShown = false;
    private static final String ONMAPCLIK_MESSAGE_SHOWN = "bMessageAfterOnMapClickShown";
    private static final String DISTANCE_SEARCH_OBSERVATIONS = "distanceSearchObservation";
    TextView NumberObs;
    private Integer number_of_meters_to_display_observations;
    Button increaseDistanceButton, decreaseDistanceButton;
    TextView labelNumberObs = null;
    public static final String PREFS_NAME = "VelobsPrefs";
    public static final String PREFS_KEY = "categories";
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "debut onCreate " + this.number_of_meters_to_display_observations);
        }

        Toast.makeText(MapActivity.this, "Si des observations sont proches, elles apparaîtront avec une icône jaune. Cliquez dessus pour accéder aux détails et les mettre à jour, sinon, cliquez sur le bouton\"Créer une nouvelle observation\"", Toast.LENGTH_LONG).show();
        context = this;
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "debut onCreate " + this.number_of_meters_to_display_observations);
        }
        this.setTitle(context.getString(R.string.title_activity_map));
        setContentView(R.layout.activity_map);

        labelNumberObs = (TextView) findViewById(R.id.numberObsLabel);



        increaseDistanceButton = (Button) findViewById(R.id.numberObsButtonIncrease);
        increaseDistanceButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (number_of_meters_to_display_observations + Integer.parseInt(context.getString(R.string.distance_to_search_observation_in_meters_step)) <= Integer.parseInt(context.getString(R.string.distance_to_search_observation_in_meters_max))) {
                    number_of_meters_to_display_observations += Integer.parseInt(context.getString(R.string.distance_to_search_observation_in_meters_step));
                    labelNumberObs.setText(context.getString(R.string.map_number_of_observations_label, Integer.toString(number_of_meters_to_display_observations)));
                    if (VelobsSingleton.getInstance().lati != null) {
                        if (circle != null){
                            circle.setRadius(number_of_meters_to_display_observations);
                        }
                        checkProxPoi(Double.parseDouble(VelobsSingleton.getInstance().lati), Double.parseDouble(VelobsSingleton.getInstance().longi));
                    }
                }
                else{
                    Toast.makeText(MapActivity.this, "Les observations ne peuvent être récupérées que dans un rayon maximal de "+ context.getString(R.string.distance_to_search_observation_in_meters_max)+"m.", Toast.LENGTH_LONG).show();

                }
            }
        });
        decreaseDistanceButton = (Button) findViewById(R.id.numberObsButtonDecrease);
        decreaseDistanceButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (number_of_meters_to_display_observations - Integer.parseInt(context.getString(R.string.distance_to_search_observation_in_meters_step)) > 5) {
                    number_of_meters_to_display_observations -= Integer.parseInt(context.getString(R.string.distance_to_search_observation_in_meters_step));
                    //labelNumberObs.setText(context.getString(R.string.map_number_of_observations_label) + " " + Integer.toString(number_of_meters_to_display_observations) + "m : ");
                    labelNumberObs.setText(context.getString(R.string.map_number_of_observations_label, Integer.toString(number_of_meters_to_display_observations)));
                    if (VelobsSingleton.getInstance().lati != null) {
                        if (circle != null){
                            circle.setRadius(number_of_meters_to_display_observations);
                        }
                        checkProxPoi(Double.parseDouble(VelobsSingleton.getInstance().lati), Double.parseDouble(VelobsSingleton.getInstance().longi));
                    }
                }
                else{
                    Toast.makeText(MapActivity.this, "Les observations doivent être récupérées dans un rayon minimal de 5m.", Toast.LENGTH_LONG).show();

                }
            }
        });

        NumberObs = (TextView) findViewById(R.id.numberObs);
        Button newObsButton = (Button) findViewById(R.id.newObsButton);
        newObsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "newObsButton.setOnClickListener, onClick");
                }
                if (leMarker != null) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "newObsButton.onClick, leMarker != null");
                    }
                    VelobsSingleton.getInstance().lati = String.valueOf(leMarker.getPosition().latitude);
                    VelobsSingleton.getInstance().longi = String.valueOf(leMarker.getPosition().longitude);
                    VelobsSingleton.getInstance().typeGeoLoc = "gps";
                    if (categoriesDownloaded && doublonsDownloaded) {

                        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                        editor = settings.edit();

                        editor.putString(PREFS_KEY, categories);
                        editor.commit();
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "newObsButton.onClick, leMarker != null, categoriesDownloaded && doublonsDownloaded " + categories);
                        }

                        Intent myIntent = new Intent(MapActivity.this, CategoryActivity.class);
                        myIntent.putExtra("CATEGORIES", categories);
                        MapActivity.this.startActivity(myIntent);
                    } else {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "newObsButton.onClick, leMarker != null, else categoriesDownloaded && doublonsDownloaded, on utilise les catégories qui ont déjà été enregistrées en local si jamais");
                        }
                        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                        categories = settings.getString(PREFS_KEY, null);
                        if (categories != null && !categories.equals("")){
                            Toast.makeText(context, "Les catégories ne semblent pas encore récupérées du serveur. On utilise les catégories stockées localement.", Toast.LENGTH_LONG).show();

                            Intent myIntent = new Intent(MapActivity.this, CategoryActivity.class);
                            myIntent.putExtra("CATEGORIES", categories);
                            MapActivity.this.startActivity(myIntent);
                        }else {
                            Toast.makeText(context, "Les catégories ne semblent pas encore récupérées du serveur. Vous ne pouvez pas créer de nouvelle observation. Assurez-vous que le réseau est bien accessible.", Toast.LENGTH_LONG).show();
                        }
                        if ((VelobsSingleton.getInstance().lati == null) && (VelobsSingleton.getInstance().longi == null)) {
                            Toast.makeText(context, "Aucune localisation n'est renseignée.", Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    VelobsSingleton.getInstance().lati = null;
                    VelobsSingleton.getInstance().longi = null;
                    Toast.makeText(context, "Aucun lieu n'a été choisi", Toast.LENGTH_LONG).show();
                }
            }
        });
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "debut onCreate " + this.number_of_meters_to_display_observations);
        }
        loadCat();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "debut onCreate " + this.number_of_meters_to_display_observations);
        }
    }

    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onPause.");
        }
        if (locationManager != null){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Dans onPause, suppression des mises à jour GPS.");
            }
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onMapClick(LatLng arg0) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onMapClick");
            Log.d(TAG, "Dans onMapClick, latitude = " + Double.toString(arg0.latitude));
            Log.d(TAG, "Dans onMapClick, longitude = " + Double.toString(arg0.longitude));
        }
        mMap.animateCamera(CameraUpdateFactory.newLatLng(arg0));
        if (leMarker != null) {
            leMarker.remove();
        }
        VelobsSingleton.getInstance().lati = Double.toString(arg0.latitude);
        VelobsSingleton.getInstance().longi = Double.toString(arg0.longitude);
        circle.setCenter(arg0);
        leMarker = mMap.addMarker(new MarkerOptions()
                .position(arg0)
                .draggable(true)
                .snippet("Vous pouvez mettre à jour une observation aux alentours ou en créer une nouvelle"));
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onMapClick, avant checkProxPoi");
        }
        checkProxPoi(arg0.latitude, arg0.longitude);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onMapClick, après checkProxPoi.");
        }
        //si l'utilisateur a cliqué sur la caret, signifie que la position donnée par le GPS devait être changée

        if (locationManager != null){
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Dans onMapClick, suppression des mises à jour GPS.");
            }
            locationManager.removeUpdates(this);
        }
       if (!this.bMessageAfterOnMapClickShown) {
           String messageInformation = "Vous pouvez mettre à jour une observation aux alentours ou en créer une nouvelle.";
           if (isGPS) {
               messageInformation += " Vous venez de déplacer le marqueur manuellement, la mise à jour de la position du marqueur par le GPS a donc été désactivée.";
           }

           Toast.makeText(context, messageInformation, Toast.LENGTH_LONG).show();
           this.bMessageAfterOnMapClickShown = true;
       }
    }


    private void setUpMapIfNeeded() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans setUpMapIfNeeded.");
        }
        if (mMap == null) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Dans setUpMapIfNeeded, mMap == null");
            }
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap gMap) {
                    mMap = gMap;
                    setUpMap();

                }
            });
        }
    }

    private void setUpMap() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans setUpMap.");
        }
        if (mMap != null) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Dans setUpMap, mMap != null");
            }
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            }

            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            mMap.setOnMapClickListener(this);
            mMap.setOnMarkerClickListener(this);
            mMap.getUiSettings().setZoomControlsEnabled(true);

            mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker arg0) {
                }

                @SuppressWarnings("unchecked")
                @Override
                public void onMarkerDragEnd(Marker arg0) {
                    Log.d(TAG, "onMarkerDragEnd...");
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(arg0.getPosition()));
                    circle.setCenter(arg0.getPosition());
                    VelobsSingleton.getInstance().lati = String.valueOf(arg0.getPosition().latitude);
                    VelobsSingleton.getInstance().longi = String.valueOf(arg0.getPosition().longitude);
                    checkProxPoi(Double.parseDouble(VelobsSingleton.getInstance().lati), Double.parseDouble(VelobsSingleton.getInstance().longi));
                }

                @Override
                public void onMarkerDrag(Marker arg0) {
                }
            });
            Double initialLatitude;
            Double initialLongitude;
            if ((VelobsSingleton.getInstance().lati == null) || (VelobsSingleton.getInstance().longi == null)) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Dans setUpMap, VelobsSingleton.getInstance().lati == null");
                }
                Toast.makeText(context, "VelObs n'a pas réussi à obtenir de localisation via le GPS ou le réseau. On affiche la position par défaut. Déplacer le marqueur rouge en tapant à l'endroit de votre observation SVP.", Toast.LENGTH_LONG).show();
                initialLatitude = Double.parseDouble(context.getString(R.string.initial_latitude));
                VelobsSingleton.getInstance().lati = context.getString(R.string.initial_latitude);
                initialLongitude = Double.parseDouble(context.getString(R.string.initial_longitude));
                VelobsSingleton.getInstance().longi = context.getString(R.string.initial_longitude);
            } else {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Dans setUpMap, VelobsSingleton.getInstance().lati != null " + VelobsSingleton.getInstance().lati + ","+ context.getString(R.string.initial_latitude));
                }
                initialLatitude = Double.parseDouble(VelobsSingleton.getInstance().lati);
                initialLongitude = Double.parseDouble(VelobsSingleton.getInstance().longi);
            }
            CameraPosition positionInit =
                    new CameraPosition.Builder()
                            .target(new LatLng(initialLatitude, initialLongitude))
                            .zoom(Float.valueOf(context.getString(R.string.initial_zoom)))
                            .build();
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(positionInit));
            leMarker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(initialLatitude, initialLongitude))
                    .draggable(true));

            circle = mMap.addCircle(new CircleOptions()
                    .center(new LatLng(initialLatitude, initialLongitude))
                    .radius(number_of_meters_to_display_observations)
                    .strokeColor(Color.YELLOW)
                    .strokeWidth(2)
                    );
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Dans setUpMap, avant checkProxPoi");
            }
            checkProxPoi(initialLatitude, initialLongitude);
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Dans setUpMap, après checkProxPoi");
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onMapReady");
        }
        mMap = googleMap;
        setUpMap();
    }

    /**
     * @param lat
     * @param lng
     * Look for observations near this position and display them on the map
     */
    private void checkProxPoi(double lat, double lng) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans checkProxPoi");
        }
        final String latPara = String.valueOf(lat);
        final String lngPara = String.valueOf(lng);

        Thread sendProcess = new Thread(new Runnable() {

            public void run() {
                Looper.prepare();
                InputStream is;

                try {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "checkProxPoi, Recherche d'observations proches");
                    }
                    msg = null;
                    String progressBarData = "Recherche d'observations proches ...";
                    DialogInterface.OnCancelListener mProgressCanceled = new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface arg0) {


                        }
                    };
                    /*mProgressDialog = ProgressDialog.show(context, "Veuillez patienter",
                            "Recherche des enregistrements proches ...", true, true, mProgressCanceled);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "checkProxPoi, avant mHandler.obtainMessage");
                    }
                    msg = mHandler.obtainMessage(MSG_IND,progressBarData);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "checkProxPoi, avant  mHandler.sendMessage(msg);");
                    }
                    mHandler.sendMessage(msg);


                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "checkProxPoi, avant HttpURLConnection()");
                    }*/

                    HttpURLConnection con;
                    URL url;


                    url = new URL(context.getString(R.string.url_servername) + "/lib/php/mobile/checkProxPoi.php");
                    con = (HttpURLConnection) url.openConnection();
                    con.setReadTimeout(Integer.parseInt(context.getString(R.string.http_request_timeout)) /* milliseconds */);
                    con.setConnectTimeout(15000 /* milliseconds */);
                    con.setRequestMethod("POST");
                    con.setDoInput(true);
                    con.setDoOutput(true);

                    StringBuilder sb = new StringBuilder();
                    //adding some data to send along with the request to the server
                    sb.append("lat=").append(latPara);
                    sb.append("&lng=").append(lngPara);
                    sb.append("&buffer=").append(Integer.toString(number_of_meters_to_display_observations));
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "checkProxPoi, envoi serveur sb.toString() = " + sb.toString());
                    }
                    // Start the query
                    con.connect();
                    OutputStreamWriter wr = new OutputStreamWriter(con
                            .getOutputStream());
                    // this is were we're adding post data to the request
                    wr.write(sb.toString());
                    wr.flush();

                    is = con.getInputStream();
                    wr.close();
                    BufferedReader reader;
                    reader = new BufferedReader(
                            new InputStreamReader(is, "UTF-8"), 8);
                    StringBuilder sb2 = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb2.append(line).append("\n");
                        if (BuildConfig.DEBUG) {
                            Log.i(TAG, "Server response = " + line);
                        }
                    }
                    is.close();
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "checkProxPoi, retour serveur sb2.toString() = " + sb2.toString());
                    }
                    responseProxPoi = sb2.toString();
                    /*msg = mHandler
                            .obtainMessage(MSG_CNF,
                                    "");
                    mHandler.sendMessage(msg);*/
                    addCloseToMarkers();
                } catch (UnsupportedEncodingException e) {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "checkProxPoi, UnsupportedEncodingException " + e.toString());
                    }
                    //e.printStackTrace();
                } catch (IOException e) {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "checkProxPoi, IOException " + e.toString());
                    }
                    //e.printStackTrace();
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "checkProxPoi, Exception " + e.toString());
                    }
                    //e.printStackTrace();
                }
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Sortie checkProxPoi");
                }
            }
        });
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "checkProxPoi, avant sendProcess.start() ");
        }
        sendProcess.start();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "checkProxPoi, apres sendProcess.start() ");
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "dans onMarkerClick");
        }
        try {
            if (marker.getTitle() != "") {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "dans onMarkerClick, marker.getTitle() != \"\"");
                }
                if (hashObservations.get(Integer.parseInt(marker.getTitle())) != null) {

                    VelobsSingleton.getInstance().poi = hashObservations.get(Integer.parseInt(marker.getTitle()));
                    if (locationManager != null){
                        locationManager.removeUpdates(this);
                    }
                    Intent myIntent = new Intent(MapActivity.this,
                            ReviewProximityPoiActivity.class);
                    MapActivity.this.startActivity(myIntent);
                }
            }
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "onMarkerClick, exception " + e.toString());
            }
        }


        return true;
    }

    /**
     * Displays marker on the map
     */
    private void addCloseToMarkers() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "dans addCloseToMarkers");
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "dans addCloseToMarkers, runOnUiThread");
                    }
                    DocumentBuilderFactory fabrique = DocumentBuilderFactory
                            .newInstance();

                    DocumentBuilder constructeur = fabrique.newDocumentBuilder();
                    Document document = constructeur.parse(new InputSource(new ByteArrayInputStream(responseProxPoi.trim().getBytes("utf-8"))));
                    NodeList nListCodeRetour = document.getElementsByTagName("coderetour");
                    if (nListCodeRetour.getLength() == 1) {

                        String codeRetourString = ((Element) nListCodeRetour.item(0))
                                .getAttribute("result");

                        int codeRetour = Integer.parseInt(codeRetourString);
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "dans addCloseToMarkers, runOnUiThread, code retour = " + Integer.toString(codeRetour));
                        }
                        if (codeRetour == -2) {
                            Toast.makeText(MapActivity.this, "Il semble que vous soyez en dehors de la zone couverte par VelObs.", Toast.LENGTH_LONG).show();
                        }if (codeRetour == -1) {
                            Toast.makeText(MapActivity.this, "Il semble que le point de référence ne soit pas positionné.", Toast.LENGTH_LONG).show();
                        }else if (codeRetour >= 0){
                            NumberObs.setText(Integer.toString(codeRetour));
                           // Toast.makeText(MapActivity.this, codeRetour + " observation(s) à proximité, cliquez sur les marqueurs pour voir le détail et les mettre à jour, sinon, cliquez sur le bouton\"Créer une nouvelle observation\"", Toast.LENGTH_LONG).show();
                            NodeList nListPoi = document.getElementsByTagName("poi");
                            for (int i = 0; i < nListPoi.getLength(); i++) {
                                if (BuildConfig.DEBUG) {
                                    Log.d(TAG, "addCloseToMarkers, obs " + ((Element) nListPoi.item(i)).getAttribute("id"));
                                    Log.d(TAG, "addCloseToMarkers, obs " + ((Element) nListPoi.item(i)).getElementsByTagName("latitude").item(0).getTextContent());
                                    Log.d(TAG, "addCloseToMarkers, obs " + ((Element) nListPoi.item(i)).getElementsByTagName("longitude").item(0).getTextContent());
                                    Log.d(TAG, "addCloseToMarkers, obs " + mMap.toString());
                                }
                                ArrayList listCommentaires = new ArrayList();
                                NodeList nListComment = ((Element) nListPoi.item(i)).getElementsByTagName("comment");
                                for (int j=0;j<nListComment.getLength();j++) {
                                    Comment cmt = new Comment(
                                            ((Element) nListComment.item(i)).getAttribute("id"),
                                            ((Element) nListComment.item(j)).getElementsByTagName("textcommentaire").item(0).getTextContent(),
                                            ((Element) nListComment.item(j)).getElementsByTagName("urlphoto").item(0).getTextContent(),
                                            ((Element) nListComment.item(j)).getElementsByTagName("datecommentaire").item(0).getTextContent()
                                    );
                                    listCommentaires.add(j,cmt);
                                }
                                String reponseCollectivite = "NA";
                                if (((Element)nListPoi.item(i)).getElementsByTagName("reponseCollectivite").item(0) != null){
                                    reponseCollectivite = ((Element) nListPoi.item(i)).getElementsByTagName("reponseCollectivite").item(0).getTextContent();
                                }

                                PointOfInterest poi = new PointOfInterest(
                                        ((Element) nListPoi.item(i)).getAttribute("id"),
                                        ((Element) nListPoi.item(i)).getElementsByTagName("category").item(0).getTextContent(),
                                        ((Element) nListPoi.item(i)).getElementsByTagName("adresse").item(0).getTextContent(),
                                        ((Element) nListPoi.item(i)).getElementsByTagName("distance").item(0).getTextContent(),
                                        ((Element) nListPoi.item(i)).getElementsByTagName("status").item(0).getTextContent(),
                                        ((Element) nListPoi.item(i)).getElementsByTagName("photo").item(0).getTextContent(),
                                        ((Element) nListPoi.item(i)).getElementsByTagName("ville").item(0).getTextContent(),
                                        ((Element) nListPoi.item(i)).getElementsByTagName("desc").item(0).getTextContent(),
                                        ((Element) nListPoi.item(i)).getElementsByTagName("prop").item(0).getTextContent(),
                                        ((Element) nListPoi.item(i)).getElementsByTagName("dateCreation").item(0).getTextContent(),
                                        reponseCollectivite,
                                        listCommentaires
                                );

                                hashObservations.put(Integer.parseInt(((Element) nListPoi.item(i)).getAttribute("id")), poi);
                                if (BuildConfig.DEBUG) {
                                    Log.d(TAG, "addCloseToMarkers, avant mMap.addMarker");
                                }
                                mMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(Double.parseDouble(((Element) nListPoi.item(i)).getElementsByTagName("latitude").item(0).getTextContent()),
                                                Double.parseDouble(((Element) nListPoi.item(i)).getElementsByTagName("longitude").item(0).getTextContent())))
                                        .draggable(false)
                                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))
                                        .title(((Element) nListPoi.item(i)).getAttribute("id"))
                                        );
                            }
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "addCloseToMarkers, après mMap.addMarker");
                            }
                        }
                        doublonsDownloaded = true;

                    }


                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "addCloseToMarkers, erreur " + e.toString());
                    }
                }
            }
        });


    }

    public static final int MSG_IND = 2;
    public static final int MSG_CNF = 1;
    public static final int MSG_ERR = 0;
    protected ProgressDialog mProgressDialog;
    final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_IND:
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.setMessage(((String) msg.obj));
                    }
                    break;
                case MSG_CNF:
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }

                    break;
                case MSG_ERR:
                    if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                    }


                    break;
                default: // should never happen
                    break;
            }
        }
    };

    public void loadCat() {
        new DownloadCatTask().execute();
    }

    private static class DownloadCatTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "doInBackground");
            }
            URL url;
            BufferedReader reader = null;
            StringBuilder stringBuilder;
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "DownloadCatTask, récupération des catégories");
            }
            try {
                url = new URL(
                        context.getString(R.string.url_servername) + "/lib/php/mobile/getMobileCategory.php");
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();


                reader = new BufferedReader(new InputStreamReader(
                        connection.getInputStream()));
                stringBuilder = new StringBuilder();

                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "DownloadCatTask, récupération des catégories " + stringBuilder.toString());
                }
                categories = stringBuilder.toString().trim();
                categoriesDownloaded = true;

            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "DownloadCatTask, récupération des catégories erreur : " + e.toString());
                }

            } finally {

                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException ioe) {
                        //ioe.printStackTrace();
                    }
                }
            }

            return null;
        }
    }

    public void showSettingsAlert() {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans showSettingsAlert.");
        }
        
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Le GPS n'est pas activé");
        alertDialog.setMessage("Voulez-vous activer le GPS pour placer le marqueur à l'endroit de votre observation (nécessitera sans doute quelques secondes pour que l'actualisation se fasse)? Si vous n'activez pas le GPS, vous devrez placer le marqueur manuellement.");
        alertDialog.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Dans showSettingsAlert, setPositiveButton.onclick");
                }
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Dans showSettingsAlert, après startActivity");
                }

            }
        });

        alertDialog.setNegativeButton("Non", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans showSettingsAlert, avant alertDialog.show().");
        }
        alertDialog.show();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans showSettingsAlert, après alertDialog.show().");
        }
    }

    @SuppressLint("MissingPermission")
    public void onResume() {
        super.onResume();
        if (this.number_of_meters_to_display_observations == null) {
            this.number_of_meters_to_display_observations = Integer.parseInt(context.getString(R.string.distance_to_search_observation_in_meters));
        }
        labelNumberObs.setText(context.getString(R.string.map_number_of_observations_label, Integer.toString(number_of_meters_to_display_observations)));

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onResume.");
        }
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        //SI API LEVEL < 23, on ne demande pas de droits, ou si API LEVEL >=23 et que la permission est OK
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || VelobsSingleton.getInstance().GPSPermissionOK) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Dans onResume, GPSPermissionOK");
            }
            //si le GPS est activé
            if (isGPS){
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Dans onResume, GPS on");
                }
                if (!this.bMessageAfterOnMapClickShown) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Dans onResume, GPS on, !this.bMessageAfterOnMapClickShown : on active les mises à jour GPS");
                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Dans onResume, GPS on, locationManager != null");
                    }
                    loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (loc != null) {
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "Dans onResume, GPS on, loc != null, on met VelobsSingleton.getInstance().lati = String.valueOf(loc.getLatitude());");
                        }
                        VelobsSingleton.getInstance().lati = String.valueOf(loc.getLatitude());
                        VelobsSingleton.getInstance().longi = String.valueOf(loc.getLongitude());
                        if (leMarker != null) {
                            leMarker.setPosition(new LatLng(loc.getLatitude(), loc.getLongitude()));
                            circle.setCenter(new LatLng(loc.getLatitude(), loc.getLongitude()));
                        }
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "GPS on, locationManager != null, loc != null ");
                            Log.d(TAG, "GPS on, latitude = " + String.valueOf(loc.getLatitude()));
                            Log.d(TAG, "GPS on, longitude = " + String.valueOf(loc.getLongitude()));
                        }
                    }
                }


            }else{
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Dans onResume, GPS off, on demande à l'utilisateur de l'activer si pas déjà demandé");
                }
                if (!this.bUserAnswerGPSToActivateAsked) {
                    this.bUserAnswerGPSToActivateAsked = true;
                    showSettingsAlert();
                }
            }
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Dans onResume, GPSPermissionOK pas OK");
            }
            if (VelobsSingleton.getInstance().lati == null){
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Dans onResume, GPSPermissionOK pas OK, VelobsSingleton.getInstance().lati == null");
                }
                VelobsSingleton.getInstance().lati = context.getString(R.string.initial_latitude);
                VelobsSingleton.getInstance().longi = context.getString(R.string.initial_longitude);
            }

        }
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onResume, avant setUpMapIfNeeded");
        }
        setUpMapIfNeeded();
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onResume, après setUpMapIfNeeded");
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onLocationChanged.");
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onLocationChanged, mise à jour de VelobsSingleton.getInstance().lati");
        }
            VelobsSingleton.getInstance().lati = String.valueOf(location.getLatitude());
            VelobsSingleton.getInstance().longi = String.valueOf(location.getLongitude());
            if (leMarker != null) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Dans onLocationChanged, leMarker != null");
                }
                leMarker.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
                circle.setCenter(new LatLng(location.getLatitude(), location.getLongitude()));
                if (mMap != null){
                    CameraUpdate center=
                            CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude()));

                    mMap.moveCamera(center);
                    //mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new LatLng(location.getLatitude(), location.getLongitude())));
                }
            }
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onLocationChanged, avant checkProxPoi");
        }
        checkProxPoi(location.getLatitude(), location.getLongitude());
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onLocationChanged, après checkProxPoi");
        }
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "GPS on, locationManager != null, loc != null ");
                Log.d(TAG, "GPS on, latitude = " + String.valueOf(location.getLatitude()));
                Log.d(TAG, "GPS on, longitude = " + String.valueOf(location.getLongitude()));
            }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onStatusChanged, on ne fait rien de particulier");
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onProviderEnabled, on ne fait rien de particulier");
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Dans onProviderDisabled, on ne fait rien de particulier");
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (BuildConfig.DEBUG){
            Log.d(TAG, "onSaveInstanceState");
        }
        outState.putBoolean(ASKED_USER_ACTIVATE_GPS, this.bUserAnswerGPSToActivateAsked);
        outState.putBoolean(ONMAPCLIK_MESSAGE_SHOWN, this.bMessageAfterOnMapClickShown);
        if (this.number_of_meters_to_display_observations != null){
            if (BuildConfig.DEBUG){
                Log.d(TAG, "onSaveInstanceState, this.number_of_meters_to_display_observations = " + this.number_of_meters_to_display_observations);
            }
            outState.putInt(DISTANCE_SEARCH_OBSERVATIONS, this.number_of_meters_to_display_observations);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG){
            Log.d(TAG, "onRestoreInstanceState");
        }
        super.onRestoreInstanceState(savedInstanceState);
        this.bUserAnswerGPSToActivateAsked = savedInstanceState.getBoolean(ASKED_USER_ACTIVATE_GPS);
        this.bMessageAfterOnMapClickShown = savedInstanceState.getBoolean(ONMAPCLIK_MESSAGE_SHOWN);
        this.number_of_meters_to_display_observations = savedInstanceState.getInt(DISTANCE_SEARCH_OBSERVATIONS);


    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        //Permet de ne pas redémarrer l'activité au changement d'orientation
        super.onConfigurationChanged(newConfig);
    }
}

